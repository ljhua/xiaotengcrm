package com.xiaoteng.entity;


import com.xiaoteng.core.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "contact")
public class Contact extends BaseModel {

    @Column(nullable = true,name = "user_id")
    String userId;//用户id
    @Column(nullable = true,name = "family_name")
    String familyName;//姓
    @Column(nullable = true,name = "given_name")
    String givenName;//名

    @Column(nullable = true,name = "phone_number")
    String number;//电话

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }



    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }



}
