package com.xiaoteng.entity;

public interface UseInfoProjection {

    String getUserId();
    String getUserName();
    String getUserStatus();
    String getUserPhone();
    String getUserIdCard();
    String getSource();

}
