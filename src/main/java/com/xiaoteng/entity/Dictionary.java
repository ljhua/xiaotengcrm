package com.xiaoteng.entity;


import com.xiaoteng.core.model.BaseEnty;


import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 */
@Table(name = "sys_dictionary")
public class Dictionary extends BaseEnty implements Serializable {

    /*** 字典编码     */
    private String code;


    /*** 字典名称     */
    @Column(nullable = true,name = "dict_name")
    private String dictName;

    @Column(nullable = true,name = "parent_id")
    private Long parentId;

    /*** 字典类型     */
    @Column(nullable = true,name = "dict_type")
    private String dictType;

    /*** 描述     */
    @Column(nullable = true,name = "description")
    private String description;

    /*** 排序     */
    @Column(nullable = true,name = "sequence")
    private Integer sequence;

    /**
     * 是否固定， 0默认为不固定，1=固定；固定就不能再去修改了
     */
    @Column(nullable = true,name = "is_fixed")
    private Integer isFixed;

    /**
     * 状态：0=启用，1=禁用
     */
    @Column(nullable = true,name = "status")
    private Integer status;

    public Dictionary() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Integer getIsFixed() {
        return isFixed;
    }

    public void setIsFixed(Integer isFixed) {
        this.isFixed = isFixed;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getDictType() {
        return dictType;
    }

    public void setDictType(String dictType) {
        this.dictType = dictType;
    }

    public String getDictName() {
        return dictName;
    }

    public Dictionary setDictName(String dictName) {
        this.dictName = dictName;
        return this;
    }
}
