package com.xiaoteng.entity;


import com.xiaoteng.core.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 记录用户授权过程
 */
@Entity
@Table(name = "authentication_info")
public class AuthenticationInfo extends BaseModel {


    @Column(nullable = false,name = "userid")
    private String userId;
    @Column(nullable = false,name = "isregister")
    private String isRegister;//是否注册
    @Column(nullable = false,name = "isauthcarrieroperatortype")
    private String isAuthCarrieroperatorType;//是否运营商授权
    @Column(nullable = false,name = "islivetype")
    private String isLiveType;//是否活体验证
    @Column(nullable = false,name = "ispersontype")
    private String isPersonType;//人工授权

    public String getIsPersonType() {
        return isPersonType;
    }

    public void setIsPersonType(String isPersonType) {
        this.isPersonType = isPersonType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }



    public String getIsRegister() {
        return isRegister;
    }

    public void setIsRegister(String isRegister) {
        this.isRegister = isRegister;
    }

    public String getIsAuthCarrieroperatorType() {
        return isAuthCarrieroperatorType;
    }

    public void setIsAuthCarrieroperatorType(String isAuthCarrieroperatorType) {
        this.isAuthCarrieroperatorType = isAuthCarrieroperatorType;
    }

    public String getIsLiveType() {
        return isLiveType;
    }

    public void setIsLiveType(String isLiveType) {
        this.isLiveType = isLiveType;
    }



}
