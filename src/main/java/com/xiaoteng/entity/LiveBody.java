package com.xiaoteng.entity;


import com.xiaoteng.core.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Table(name = "live_body")
public class LiveBody  extends BaseModel {

    @Column(nullable = false,name = "user_id")
    String userId;

    @Column(nullable = false,name = "user_name")
    String userName;

    @Column(nullable = true,name = "idcard")
    String idCard;

    @Column(nullable = true,name = "code")
    String code;

    @Column(nullable = true,name = "passed")
    boolean passed;

    @Column(nullable = true,name = "liveness_score")
    BigDecimal livenessScore;

    @Column(nullable = true,name = "verification_score")
    BigDecimal verificationScore;
    @Column(nullable = true,name = "image_id")

    String imageId;
    @Column(nullable = true,name = "image_timestamp")
    String imageTimestamp;

    @Column(nullable = true,name = "base64_image")
    @Size(max = 900, min = 0, message = " 长度必须大于等于0且小于等于900")
    String base64Image;

    @Column(nullable = true,name = "request_id")
    String requestId;



    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public BigDecimal getLivenessScore() {
        return livenessScore;
    }

    public void setLivenessScore(BigDecimal livenessScore) {
        this.livenessScore = livenessScore;
    }

    public BigDecimal getVerificationScore() {
        return verificationScore;
    }

    public void setVerificationScore(BigDecimal verificationScore) {
        this.verificationScore = verificationScore;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImageTimestamp() {
        return imageTimestamp;
    }

    public void setImageTimestamp(String imageTimestamp) {
        this.imageTimestamp = imageTimestamp;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }



}
