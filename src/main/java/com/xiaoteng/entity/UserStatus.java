package com.xiaoteng.entity;

import com.xiaoteng.core.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_status")
public class UserStatus {

    @Column(nullable = true,name = "username")
    String userName;
    @Column(nullable = true,name = "userphone")
    String userPhone;
    @Column(nullable = true,name = "userstatus")
    String userStatus;

    @Id
    @Column(nullable = false,name = "userid")
    String userId;

    public String getUserName() {
        return userName;
    }

    public UserStatus setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public UserStatus setUserPhone(String userPhone) {
        this.userPhone = userPhone;
        return this;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public UserStatus setUserStatus(String userStatus) {
        this.userStatus = userStatus;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public UserStatus setUserId(String userId) {
        this.userId = userId;
        return this;
    }






}
