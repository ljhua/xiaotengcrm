package com.xiaoteng.entity;


import com.xiaoteng.core.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 还款
 */
@Entity
@Table(name = "repayment")
public class Repayment extends BaseModel {


    @Column(nullable = false,name = "loan_code")
    String loanCode;//借款流水号
    @Column(nullable = false,name = "user_id")
    String userId;
    @Column(nullable = true,name = "repayment_method")
    String repaymentMethod;//还款方式
    @Column(nullable = false,name = "repayment_bank_info")
    String pepaymentBankInfo;//还款银行账号
    @Column(nullable = false,name = "repayment_amount")
    double pepaymentAmount;//还款金额

    public String getPepaymentBankInfo() {
        return pepaymentBankInfo;
    }

    public void setPepaymentBankInfo(String pepaymentBankInfo) {
        this.pepaymentBankInfo = pepaymentBankInfo;
    }

    public double getPepaymentAmount() {
        return pepaymentAmount;
    }

    public void setPepaymentAmount(double pepaymentAmount) {
        this.pepaymentAmount = pepaymentAmount;
    }

    public String getLoanCode() {
        return loanCode;
    }

    public void setLoanCode(String loanCode) {
        this.loanCode = loanCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRepaymentMethod() {
        return repaymentMethod;
    }

    public void setRepaymentMethod(String repaymentMethod) {
        this.repaymentMethod = repaymentMethod;
    }


}
