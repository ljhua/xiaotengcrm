package com.xiaoteng.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.xiaoteng.core.model.BaseEnty;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author Hao
 * @since 2017-07-05
 */
@Entity
@Table(name = "sys_resource")
public class Resource extends BaseEnty implements Serializable {

    /*** 权限编码     */
    @Column(nullable = true,name = "code")
    private String code;

    /*** 名称     */
    @Column(nullable = true,name = "name")
    private String name;
    /**
     *easyui tree的父级节点
     **/
    @JSONField(name="_parentId")//
    @Column(nullable = true,name = "parent_id")
    private Long parentId;

    /*** 类型     */
    @Column(nullable = true,name = "type")
    private String type;

    /*** Url路径     */
    @Column(nullable = true,name = "url")
    private String url;

    /*** 菜单按钮     */
    @Column(nullable = true,name = "icon")
    private String icon;

    /*** 描述     */
    @Column(nullable = true,name = "description")
    private String description;

    /*** 排序     */
    @Column(nullable = true,name = "sequence")
    private Integer sequence;

    /**
     * 是否固定， 0默认为不固定，1=固定；固定就不能再去修改了
     */
    @Column(nullable = true,name = "is_fixed")
    private Integer isFixed;

    /**
     * 状态：0=启用，1=禁用
     */
    @Column(nullable = true,name = "status")
    private Integer status;

    public Resource() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Integer getIsFixed() {
        return isFixed;
    }

    public void setIsFixed(Integer isFixed) {
        this.isFixed = isFixed;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
}
