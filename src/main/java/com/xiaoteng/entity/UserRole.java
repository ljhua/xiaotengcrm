package com.xiaoteng.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 */
@Entity
@Table(name = "sys_user_role")
public class UserRole implements Serializable {

    private static final long serialVersionUID = 1L;
	@Id
	@Column(nullable = true,name = "user_id")
	private Long userId;
	@Id
	@Column(nullable = true,name = "role_id")
	private Long roleId;


	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	@Override
	public String toString() {
		return "UserRole{" +
			"userId=" + userId +
			", roleId=" + roleId +
			"}";
	}
}
