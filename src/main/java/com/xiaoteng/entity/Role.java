package com.xiaoteng.entity;

import com.xiaoteng.core.model.BaseEnty;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 */
@Entity
@Table(name = "sys_role")
public class Role extends BaseEnty implements Serializable {

    /*** 角色编码     */
    @Column(nullable = true,name = "code")
    private String code;

    /*** 角色名称     */
    @Column(nullable = true,name = "name")
    private String name;

    /*** 描述     */
    @Column(nullable = true,name = "description")
    private String description;

    /**
     * 是否固定， 0默认为不固定，1=固定；固定就不能再去修改了
     */

    @Column(nullable = true,name = "is_fixed")
    private Integer isFixed;

    /**
     * 状态：0=启用，1=禁用
     */
    @Column(nullable = true,name = "status")
    private Integer status;

    public Role() {
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIsFixed() {
        return isFixed;
    }

    public void setIsFixed(Integer isFixed) {
        this.isFixed = isFixed;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
