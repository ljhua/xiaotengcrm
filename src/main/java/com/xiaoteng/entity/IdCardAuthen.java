package com.xiaoteng.entity;


import com.xiaoteng.core.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "id_card_authen")
public class IdCardAuthen  extends BaseModel {

    @Column(nullable = false,name = "user_id")
    String userId;
    @Column(nullable = true,name = "user_name")
    String name;
    @Column(nullable = true,name = "year")
    String year;
    @Column(nullable = true,name = "month")
    String month;
    @Column(nullable = true,name = "day")
    String day;
    @Column(nullable = true,name = "number")
    String number;
    @Column(nullable = true,name = "nation")
    String nation;
    @Column(nullable = true,name = "gender")
    String gender;
    @Column(nullable = true,name = "address")
    String address;
    @Column(nullable = true,name = "request_id")
    String requestId;
    @Column(nullable = true,name = "authority")
    String authority;

    @Column(nullable = true,name = "back_request_id")
    String backRequestId;
    @Column(nullable = true,name = "time_limit")
    String timeLimit;

    public String getBackRequestId() {
        return backRequestId;
    }

    public void setBackRequestId(String backRequestId) {
        this.backRequestId = backRequestId;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }



    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }


}
