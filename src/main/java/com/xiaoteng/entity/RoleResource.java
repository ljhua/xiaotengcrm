package com.xiaoteng.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 */
@Entity
@Table(name = "sys_role_resource")
public class RoleResource implements Serializable {

    private static final long serialVersionUID = 1L;
	@Id
	@Column(nullable = true,name = "role_id")
	private Long roleId;
	@Id
	@Column(nullable = true,name = "resource_id")
	private Long resourceId;


	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	@Override
	public String toString() {
		return "RoleResource{" +
			"roleId=" + roleId +
			", resourceId=" + resourceId +
			"}";
	}
}
