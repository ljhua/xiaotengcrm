package com.xiaoteng.entity;


import com.xiaoteng.core.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "emergency_person")
public class EmergencyPerson extends BaseModel {

    @Column(nullable = false,name = "user_id")
    String userId;
    @Column(nullable = false,name = "user_name")
    String name;
    @Column(nullable = false,name = "relation_ship")
    String relationShip;
    @Column(nullable = false,name = "mobile")
    String mobile;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelationShip() {
        return relationShip;
    }

    public void setRelationShip(String relationShip) {
        this.relationShip = relationShip;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


}
