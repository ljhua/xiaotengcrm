package com.xiaoteng.entity;


import com.xiaoteng.core.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "base_user")
public class BaseUser extends BaseModel {

    @Column(nullable = true,name = "username")
    private String userName;
    @Column(nullable = false,name = "telphonenumber")
    private String telphoneNumber;
    @Column(nullable = true,name = "idcard")
    private String idCard;
    @Column(nullable = false,name = "password")
    private String password;
    @Column(nullable = true,name = "useage")
    private String useAge;

    @Column(nullable = true,name = "source")
    private String source;

    public String getSource() {
        return source;
    }

    public BaseUser setSource(String source) {
        this.source = source;
        return this;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTelphoneNumber() {
        return telphoneNumber;
    }

    public void setTelphoneNumber(String telphoneNumber) {
        this.telphoneNumber = telphoneNumber;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getUseAge() {
        return useAge;
    }

    public void setUseAge(String useAge) {
        this.useAge = useAge;
    }
   public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
