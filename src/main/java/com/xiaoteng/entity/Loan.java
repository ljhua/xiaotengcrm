package com.xiaoteng.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xiaoteng.core.model.BaseModel;

import javax.persistence.*;
import java.util.Date;

/**
 * 借款业务表
 */
@Entity
@Table(name = "loan")
public class Loan extends BaseModel {


    @Column(nullable = false,name = "loan_code")
    String loanCode;//借款流水号
    @Column(nullable = false,name = "user_id")
    String userId;//用户id
    @Column(nullable = true,name = "loan_purpose")
    String loanPurpose;//用途
    @Column(nullable = false,name = "interest")
    double interest;//利息
    @Column(nullable = false,name = "loan_amount")
    double loanAmount;//借款金额
    @Column(nullable = false,name = "life_of_loan")
    int lifeOfLoan;//借款期限 7 14 30
    @Column(nullable = true,name = "loan_type")
    String loanType;//借款渠道 微信 app web
    @Column(nullable = false,name = "state")
    String state;//状态 0 申请中待审核，1 审核通过未放款 2 审核未通过 3 已放款 4已还款
    @Column(nullable = true,name = "loan_bank_info")
    String loanBankInfo;//借款银行账号
    @JSONField(format = "yyyy-MM-dd HH:mm")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false,name = "loan_end_date")
    Date loanEndDate;//还款日期
    @Column(nullable = false,name = "repayment")
    double repayment;//还款金额

    public double getRepayment() {
        return repayment;
    }

    public Loan setRepayment(double repayment) {
        this.repayment = repayment;
        return this;
    }


    public Date getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(Date loanEndDate) {
        this.loanEndDate = loanEndDate;
    }



    public String getLoanCode() {
        return loanCode;
    }

    public void setLoanCode(String loanCode) {
        this.loanCode = loanCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoanPurpose() {
        return loanPurpose;
    }

    public void setLoanPurpose(String loanPurpose) {
        this.loanPurpose = loanPurpose;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public double getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(double loanAmount) {
        this.loanAmount = loanAmount;
    }

    public int getLifeOfLoan() {
        return lifeOfLoan;
    }

    public void setLifeOfLoan(int lifeOfLoan) {
        this.lifeOfLoan = lifeOfLoan;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLoanBankInfo() {
        return loanBankInfo;
    }

    public void setLoanBankInfo(String loanBankInfo) {
        this.loanBankInfo = loanBankInfo;
    }



}
