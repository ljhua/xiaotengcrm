package com.xiaoteng.entity;


import com.xiaoteng.core.model.BaseEnty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 */
@Entity
@Table(name = "sys_user")
public class User extends BaseEnty implements Serializable {

    /*** 密码     */
    @Column(nullable = true,name = "password")
    private String password;

    /*** 渠道名称     */
    @Column(nullable = true,name = "name")
    private String name;

    /*** 手机号     */
    @Column(nullable = true,name = "mobile")
    private String mobile;

    /**
     * 状态：0=启用，1=禁用
     */
    @Column(nullable = true,name = "status")
    private Integer status=0;

    /*** 是否扣点    */
    @Column(nullable = true,name = "is_deduction")
    private int isDeduction=0;

    /*** 扣点    */
    @Column(nullable = true,name = "deduction_ratio")
    private BigDecimal deductionRatio=new BigDecimal(0.00);

    /*** 渠道地址    */
    @Column(nullable = true,name = "address")
    private String address;

    /*** 推广员   */
    @Column(nullable = true,name = "promoters")
    private String promoters;


    public int getIsDeduction() {
        return isDeduction;
    }

    public void setIsDeduction(int isDeduction) {
        this.isDeduction = isDeduction;
    }

    public BigDecimal getDeductionRatio() {
        return deductionRatio;
    }

    public void setDeductionRatio(BigDecimal deductionRatio) {
        this.deductionRatio = deductionRatio;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }



    public String getPromoters() {
        return promoters;
    }

    public void setPromoters(String promoters) {
        this.promoters = promoters;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
