package com.xiaoteng.entity;


import com.xiaoteng.core.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 运营商授权--采集数据成功
 */
@Entity
@Table(name = "authentication_bill")
public class AuthenticationBill extends BaseModel {


    @Column(nullable = false,name = "mobile")
    private String mobile;
    @Column(nullable = false,name = "userid")
    private String user_id;
    @Column(nullable = false,name = "taskid")
    private String task_id;

    @Column(nullable = true,name = "idcard")
    private String idcard;
    @Column(nullable = true,name = "message")
    private String message;
    /*   @Column(nullable = false,name = "bills")
       private String bills;*/
    @Column(nullable = false,name = "name")
    private String name;

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

  /*  public String getBills() {
        return bills;
    }

    public void setBills(String bills) {
        this.bills = bills;
    }*/


}
