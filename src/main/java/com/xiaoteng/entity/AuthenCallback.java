package com.xiaoteng.entity;


import com.xiaoteng.core.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 运营商--任务创建通知
 */
@Entity
@Table(name = "authen_callback")
public class AuthenCallback  extends BaseModel {



    @Column(nullable = false,name = "mobile")
    private String mobile;
    @Column(nullable = false,name = "userid")
    private String user_id;
    @Column(nullable = false,name = "taskid")
    private String task_id;
    @Column(nullable = true,name = "idcard")
    private String idcard;
    @Column(nullable = false,name = "timestamp")
    private long timestamp;
    @Column(nullable = true,name = "name")
    private String name;

    @Column(nullable = false,name = "result")
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }



    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }




}
