package com.xiaoteng.entity;


import com.xiaoteng.core.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 运营商接口回调
 */
@Entity
@Table(name = "authentication_task")
public class AuthenticationTask extends BaseModel {


    @Column(nullable = false,name = "taskid")
    private String taskId;//任务id
    @Column(nullable = false,name = "userid")
    private String userId;//用户id
    @Column(nullable = false,name = "mobile")
    private String mobile;//电话号码
    @Column(nullable = false,name = "result",length=500)
    private String result;//回调结果

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


}
