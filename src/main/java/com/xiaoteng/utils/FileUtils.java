package com.xiaoteng.utils;

import java.io.File;

/**
 * @className:FileUtils
 * @author:yummy
 * @data:2019/1/23 13:35
 * @discription:删除文件工具类
 **/
public class FileUtils {

    //判断file是否存在
    public static boolean  findFile(String path){
        File file=new File(path);
        return file.exists();
    }
    public static boolean  deleteFile(File file){
        return file.delete();
    }
    public static boolean  deleteFile(String path){
       if (findFile(path)){
           File file =new File(path);
          return file.delete();
       }else {
           return false;
       }
    }
}
