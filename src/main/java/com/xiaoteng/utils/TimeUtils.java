package com.xiaoteng.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

/**
 * @className:TimeUtils
 * @author:yummy
 * @data:2019/1/18 16:41
 * @discription:class
 **/
public class TimeUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(TimeUtils.class);

    public TimeUtils() {
    }

    public static String toString(long time, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(new Date(time));
    }

    public static String toString(Date time, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(time);
    }

    public static String toString(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(new Date(time));
    }

    public static String toString(Date time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(time);
    }

    public static Long fromString(String time) {
        try {
            return time.length() == 10 ? fromString(time, "yyyy-MM-dd") : fromString(time, "yyyy-MM-dd HH:mm:ss");
        } catch (Exception var2) {
            return null;
        }
    }

    public static long fromString(String time, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Date date = null;

        try {
            date = simpleDateFormat.parse(time);
            if (date != null) {
                return date.getTime();
            }
        } catch (Exception var5) {
            ;
        }

        return -1L;
    }

    public static String getTimeDes(Long ms) {
        if (ms != null && ms != 0L) {
            boolean minus = false;
            if (ms < 0L) {
                minus = true;
                ms = -ms;
            }

            int ss = 1000;
            int mi = ss * 60;
            int hh = mi * 60;
            int dd = hh * 24;
            long day = ms / (long)dd;
            long hour = (ms - day * (long)dd) / (long)hh;
            long minute = (ms - day * (long)dd - hour * (long)hh) / (long)mi;
            long second = (ms - day * (long)dd - hour * (long)hh - minute * (long)mi) / (long)ss;
            long milliSecond = ms - day * (long)dd - hour * (long)hh - minute * (long)mi - second * (long)ss;
            StringBuilder str = new StringBuilder();
            if (day > 0L) {
                str.append(day).append("天,");
            }

            if (hour > 0L) {
                str.append(hour).append("小时,");
            }

            if (minute > 0L) {
                str.append(minute).append("分钟,");
            }

            if (second > 0L) {
                str.append(second).append("秒,");
            }

            if (milliSecond > 0L) {
                str.append(milliSecond).append("毫秒,");
            }

            if (str.length() > 0) {
                str.setLength(str.length() - 1);
            }

            return minus ? "-" + str.toString() : str.toString();
        } else {
            return "0毫秒";
        }
    }

    public static String toString(LocalDateTime time) {
        return toString(time, "yyyy-MM-dd HH:mm:ss");
    }

    public static String toString(LocalDateTime time, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return time.format(formatter);
    }

    public static LocalDateTime parse(String text) {
        return parse(text, "yyyy/MM/dd HH:mm:ss");
    }

    public static LocalDateTime parse(String text, String pattern) {
        return LocalDateTime.ofInstant((new Date(fromString(text, pattern))).toInstant(), ZoneId.systemDefault());
    }

    public static long trimToMinute(long time) {
        return fromString(toString(time, "yyyy-MM-dd HH:mm"), "yyyy-MM-dd HH:mm");
    }

    public static long trimToDay(long time) {
        return fromString(toString(time, "yyyy-MM-dd"), "yyyy-MM-dd");
    }
    public static  String getCurrentYearMonthDD() {
        TimeZone tz = TimeZone.getTimeZone("Etc/GMT-8");
        TimeZone.setDefault(tz);
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date(new Date().getTime());
        String currentDate = format.format(date);
        return currentDate;
    }
    public static void main(String[] args) {
        String time = "2015-08-16 00:00:00";
        System.out.println("time:" + time);
        long t = fromString(time);
        System.out.println("time:" + t);
        String ts = toString(t);
        System.out.println("time:" + ts);
        System.out.println(toString(new Date(), "yyyyMMddHHmm"));
        System.out.println(getTimeDes(10000L));
    }
}
