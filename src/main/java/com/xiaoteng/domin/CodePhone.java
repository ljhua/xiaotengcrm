package com.xiaoteng.domin;

import io.swagger.annotations.ApiModel;

/**
 * 验证码登录使用
 */
@ApiModel(value="CodePhone", discriminator = "验证码登录使用", subTypes = {CodePhone.class})
public class CodePhone {


    private String mobile;
    private String flag;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }


}
