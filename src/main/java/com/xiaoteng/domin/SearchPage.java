package com.xiaoteng.domin;

import io.swagger.annotations.ApiModel;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@ApiModel(value="SearchPage", discriminator = "搜索使用", subTypes = {SearchPage.class})
public class SearchPage  {
    private  int page;
    private  int size;
    private String condition;//电话号码、身份证、用户名
    private String userStatus;//用户状态
    private String source;//用户来源
    private String registerStart;//注册开始时间
    private String registerEnd;//注册结束时间

    public String getUserStatus() {
        return userStatus;
    }

    public SearchPage setUserStatus(String userStatus) {
        this.userStatus = userStatus;
        return this;
    }



    public String getSource() {
        return source;
    }

    public SearchPage setSource(String source) {
        this.source = source;
        return this;
    }

    public String getRegisterStart() {
        return registerStart;
    }

    public SearchPage setRegisterStart(String registerStart) {
        this.registerStart = registerStart;
        return this;
    }

    public String getRegisterEnd() {
        return registerEnd;
    }

    public SearchPage setRegisterEnd(String registerEnd) {
        this.registerEnd = registerEnd;
        return this;
    }


    public String getCondition() {
        return condition;
    }

    public SearchPage setCondition(String condition) {
        this.condition = condition;
        return this;
    }


    public int getPage() {
        return page;
    }

    public SearchPage setPage(int page) {
        this.page = page;
        return this;
    }

    public int getSize() {
        return size;
    }

    public SearchPage setSize(int size) {
        this.size = size;
        return this;
    }




}
