package com.xiaoteng.domin;

import io.swagger.annotations.ApiModel;


@ApiModel(value="IdCardBack", discriminator = "身份证背面信息", subTypes = {IdCardBack.class})
public class IdCardBack {


    String userId;
    String authority;
    String backRequestId;
    String timeLimit;

    public String getBackRequestId() {
        return backRequestId;
    }

    public void setBackRequestId(String backRequestId) {
        this.backRequestId = backRequestId;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }



    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

}
