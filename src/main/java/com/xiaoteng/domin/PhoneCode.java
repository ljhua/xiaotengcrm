package com.xiaoteng.domin;

import io.swagger.annotations.ApiModel;

/**
 * 注册使用
 */
@ApiModel(value="PhoneCode", discriminator = "注册使用", subTypes = {PhoneCode.class})

public class PhoneCode {

    private String phoneNumber;
    private String code;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
