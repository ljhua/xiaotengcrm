package com.xiaoteng.domin;


import javax.persistence.Column;
import java.math.BigDecimal;

/**
 *
 */

public class SourceResponse {

    private String id;

    /*** 渠道名称     */
    private String name;

    /*** 手机号     */
    private String mobile;


    /**
     * 状态：0=启用，1=禁用
     */
    private Integer status;

    /*** 是否扣点    */
    private int isDeduction;

    /*** 扣点    */
    private BigDecimal deductionRatio;

    /*** 渠道地址    */
    private String address;

    /*** 推广员   */
    private String promoters;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public int getIsDeduction() {
        return isDeduction;
    }

    public void setIsDeduction(int isDeduction) {
        this.isDeduction = isDeduction;
    }

    public BigDecimal getDeductionRatio() {
        return deductionRatio;
    }

    public void setDeductionRatio(BigDecimal deductionRatio) {
        this.deductionRatio = deductionRatio;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPromoters() {
        return promoters;
    }

    public void setPromoters(String promoters) {
        this.promoters = promoters;
    }
}
