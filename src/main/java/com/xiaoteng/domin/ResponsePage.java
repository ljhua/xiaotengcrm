package com.xiaoteng.domin;

import java.util.List;
import java.util.Map;

public class ResponsePage{

    int pageSize;//每页条数
    int number;//当前页码
    int total;//总数
    List<Map<String,Object>> content;//数据

    public int getTotal() {
        return total;
    }

    public ResponsePage setTotal(int total) {
        this.total = total;
        return this;
    }



    public int getPageSize() {
        return pageSize;
    }

    public ResponsePage setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }



    public int getNumber() {
        return number;
    }

    public ResponsePage setNumber(int number) {
        this.number = number;
        return this;
    }

    public List<Map<String, Object>> getContent() {
        return content;
    }

    public ResponsePage setContent(List<Map<String, Object>> content) {
        this.content = content;
        return this;
    }


}
