package com.xiaoteng.authorization.manager;

/**
 * Token的验证器
 * @author yummy
 * @create 2018-11-23 17:45:00
 */
public interface TokenValidator {
    public boolean validate(String token);
}
