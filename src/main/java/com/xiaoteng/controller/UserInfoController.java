package com.xiaoteng.controller;

import com.xiaoteng.authorization.annotation.Authorization;
import com.xiaoteng.authorization.annotation.CurrentUser;
import com.xiaoteng.core.controller.BaseController;
import com.xiaoteng.core.model.ApiResponse;
import com.xiaoteng.core.utils.ExcelUtil;
import com.xiaoteng.domin.ResponsePage;
import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.domin.SourceResponse;
import com.xiaoteng.domin.UserResponse;
import com.xiaoteng.entity.*;
import com.xiaoteng.entity.Contact;
import com.xiaoteng.service.*;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

@CrossOrigin(maxAge=3600)
@Api(value = "渠道接口",description = "渠道信息相关接口")
@RestController
@RequestMapping("/source")
public class UserInfoController extends BaseController {

    @Autowired
    UserService  userService;

    @Authorization
    @ApiOperation(value="渠道列表查询", notes="渠道查询")
    @RequestMapping(value = "/list",method = RequestMethod.POST)
    public ApiResponse userList(@ApiParam(name="SearchPage实体",value="json格式",required=true)@RequestBody SearchPage searchPage) {

        try {
            if (searchPage != null) {
              // Page page=baseUserService.queryByproperties(searchPage);
                ResponsePage  responsePage=userService.findUserListByproperties(searchPage);
                return super.callbackSuccess(responsePage);
            } else {
                Map<String, Object> errMap = new HashMap<>();
                errMap.put("errMessage", "参数不能为空");
                return super.callbackFail(errMap);
            }
        } catch (Exception e) {
            Map<String, Object> errMap = new HashMap<>();
            errMap.put("errMessage", "系统异常");
            return super.callbackFail(errMap);
        }

    }
    @Authorization
    @ApiOperation(value="渠道详情", notes="查询渠道信息")
    @ApiImplicitParam(name = "id", value = "渠道id", required = true,dataType = "String",paramType = "path")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ApiResponse getUserDetail(@PathVariable String id){

        try {
            if (id!=null){
                SourceResponse baseUser=userService.get(id);
                return super.callbackSuccess(baseUser);
            }else {
                Map<String,Object>errMap=new HashMap<>();
                errMap.put("errMessage","id不能为空");
                return super.callbackFail(errMap);
            }
        }catch (Exception e){
            Map<String,Object>errMap=new HashMap<>();
            errMap.put("errMessage","系统异常");
            return super.callbackFail(errMap);
        }
    }

    @ApiOperation(value="更新渠道状态", notes="更新渠道状态")
    @Authorization
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    public ApiResponse updateUser(@RequestBody User user,@CurrentUser UserResponse currentUser) {

        try {
            if (user!=null) {
                user.setUpdateUser(currentUser.getName());
                // Page page=baseUserService.queryByproperties(searchPage);
                SourceResponse user1=userService.get(user.getId());
                if (user1!=null){
                    userService.update(user);
                    Map<String, Object> errMap = new HashMap<>();
                    errMap.put("userid",user.getId());
                    return super.callbackSuccess(errMap);
                }else{
                    Map<String, Object> errMap = new HashMap<>();
                    errMap.put("errMessage", "没有找到userid："+user.getId()+"的数据");
                    return super.callbackFail(errMap);
                }

            } else {
                Map<String, Object> errMap = new HashMap<>();
                errMap.put("errMessage", "参数不能为空");
                return super.callbackFail(errMap);
            }
        } catch (Exception e) {
            Map<String, Object> errMap = new HashMap<>();
            errMap.put("errMessage", "系统异常");
            return super.callbackFail(errMap);
        }

    }

    @ApiOperation(value="更新渠道扣量", notes="更新渠道扣量")
    @Authorization
    @RequestMapping(value = "/updateUserDeduction",method = RequestMethod.POST)
    public ApiResponse updateUserDeduction(@RequestBody User user,@CurrentUser UserResponse currentUser) {

        try {
            if (user!=null) {
                user.setUpdateUser(currentUser.getName());
                // Page page=baseUserService.queryByproperties(searchPage);
                SourceResponse user1=userService.get(user.getId());
                if (user1!=null){
                    userService.updateUserDeduction(user);
                    Map<String, Object> errMap = new HashMap<>();
                    errMap.put("userid",user.getId());
                    return super.callbackSuccess(errMap);
                }else{
                    Map<String, Object> errMap = new HashMap<>();
                    errMap.put("errMessage", "没有找到userid："+user.getId()+"的数据");
                    return super.callbackFail(errMap);
                }

            } else {
                Map<String, Object> errMap = new HashMap<>();
                errMap.put("errMessage", "参数不能为空");
                return super.callbackFail(errMap);
            }
        } catch (Exception e) {
            Map<String, Object> errMap = new HashMap<>();
            errMap.put("errMessage", "系统异常");
            return super.callbackFail(errMap);
        }

    }
    @ApiOperation(value="保存渠道状态", notes="保存渠道状态")
    @Authorization
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public ApiResponse save(@RequestBody User user,@CurrentUser UserResponse currentUser) {
        try {
            if (user!=null) {
                user.setCreateUser(currentUser.getName());
                User temp= userService.getByMobile(currentUser.getMobile());
                if (temp!=null){
                    Map<String, Object> errMap = new HashMap<>();
                    errMap.put("errMessage", "该渠道已经存在");
                    return super.callbackFail(errMap);
                }else {
                    User user1= userService.save(user);
                    return super.callbackSuccess(user1);
                }

            } else {
                Map<String, Object> errMap = new HashMap<>();
                errMap.put("errMessage", "参数不能为空");
                return super.callbackFail(errMap);
            }
        } catch (Exception e) {
            Map<String, Object> errMap = new HashMap<>();
            errMap.put("errMessage", "系统异常");
            return super.callbackFail(errMap);
        }
    }

    @Authorization
    @ApiOperation(value="渠道统计查询", notes="渠道统计查询")
    @RequestMapping(value = "/statisticalAnalysisList",method = RequestMethod.POST)
    public ApiResponse statisticalAnalysisList(@ApiParam(name="SearchPage实体",value="json格式",required=true)@RequestBody SearchPage searchPage) {

        try {
            if (searchPage != null) {
                // Page page=baseUserService.queryByproperties(searchPage);
                ResponsePage  responsePage=userService.statisticalAnalysisList(searchPage);
                return super.callbackSuccess(responsePage);
            } else {
                Map<String, Object> errMap = new HashMap<>();
                errMap.put("errMessage", "参数不能为空");
                return super.callbackFail(errMap);
            }
        } catch (Exception e) {
            Map<String, Object> errMap = new HashMap<>();
            errMap.put("errMessage", "系统异常");
            return super.callbackFail(errMap);
        }

    }
}
