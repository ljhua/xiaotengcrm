package com.xiaoteng.controller;

import com.xiaoteng.core.controller.BaseController;
import com.xiaoteng.core.model.ApiResponse;
import com.xiaoteng.core.utils.CodeUtils;
import com.xiaoteng.domin.CodePhone;
import com.xiaoteng.domin.PhoneCode;
import com.xiaoteng.entity.BaseUser;
import com.xiaoteng.service.AuthenticationInfoService;
import com.xiaoteng.service.BaseUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(maxAge=3600)
@Api(value = "登录相关接口",description = "登录相关接口")
@RestController
@RequestMapping("/login")
public class LoginController extends BaseController {

    @Autowired
    BaseUserService baseUserService;
    @Autowired
    AuthenticationInfoService authenticationInfoService;

    @ApiOperation(value="密码登录", notes="根据手机号码和密码登录")
    @ApiImplicitParam(name = "phoneCode", value = "用户登录信息", required = true, dataType = "PhoneCode")
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public ApiResponse login(@ApiParam(name="phoneCode实体",value="json格式",required=true)@RequestBody PhoneCode phoneCode) {

        try {
            if (phoneCode!=null){
             return super.callbackSuccess(new BaseUser());
            }else {
                Map<String,Object>errMap=new HashMap<>();
                errMap.put("errMessage","参数不能为空");
                return super.callbackFail(errMap);
            }
        }catch (Exception e){
            Map<String,Object>errMap=new HashMap<>();
            errMap.put("errMessage","系统异常");
            return super.callbackFail(errMap);
        }


    }

    @ApiOperation(value="验证码", notes="发送验证码")
    @ApiImplicitParam(name = "mobile", value = "发送验证码", required = true,dataType = "String",paramType = "path")
    @RequestMapping(value = "/{mobile}",method = RequestMethod.GET)
    public ApiResponse sendCode(@PathVariable String mobile){

        try {
            if (mobile!=null){
                Map<String ,Object> code= CodeUtils.sendMessage(mobile);
                String codeValue=(String)code.get("code");
                HttpSession session = request.getSession();
                session.setAttribute("code",codeValue);
                return super.callbackSuccess(code);
            }else {
                Map<String,Object>errMap=new HashMap<>();
                errMap.put("errMessage","电话号码不能为空");
                return super.callbackFail(errMap);
            }
        }catch (Exception e){
            Map<String,Object>errMap=new HashMap<>();
            errMap.put("errMessage","系统异常");
            return super.callbackFail(errMap);
        }

    }

    @ApiOperation(value="验证码登录", notes="验证码登录")
    @RequestMapping(value = "/codeLogin",method = RequestMethod.POST)
    public ApiResponse codeLogin(@ApiParam(name="CodePhone实体",value="json格式",required=true)@RequestBody CodePhone codePhone) {

        try {
            if (codePhone != null) {
                if (codePhone.getFlag().equals("1")) {//验证成功
                    BaseUser baseUser = new BaseUser();
                    if (baseUser != null) {
                        return super.callbackSuccess(baseUser);
                    } else {
                        Map<String, Object> errMap = new HashMap<>();
                        errMap.put("errMessage", "该号码未注册");
                        return super.callbackFail(errMap);
                    }

                } else {//验证失败
                    Map<String, Object> errMap = new HashMap<>();
                    errMap.put("errMessage", "验证码错误");
                    return super.callbackFail(errMap);
                }

            } else {
                Map<String, Object> errMap = new HashMap<>();
                errMap.put("errMessage", "参数不能为空");
                return super.callbackFail(errMap);
            }
        } catch (Exception e) {
            Map<String, Object> errMap = new HashMap<>();
            errMap.put("errMessage", "系统异常");
            return super.callbackFail(errMap);
        }
    }

    @ApiOperation(value="修改密码", notes="修改密码")
    @RequestMapping(value = "/updatePassword",method = RequestMethod.POST)
    public ApiResponse updatePassword(@ApiParam(name="phoneCode实体",value="json格式",required=true)@RequestBody PhoneCode phoneCode) {
        try {
            if (phoneCode != null) {
                     BaseUser baseUser = new BaseUser();
                    if (baseUser != null) {
                      return super.callbackSuccess(baseUser);
                    } else {
                        Map<String, Object> errMap = new HashMap<>();
                        errMap.put("errMessage", "该号码未注册");
                        return super.callbackFail(errMap);
                    }

            } else {
                Map<String, Object> errMap = new HashMap<>();
                errMap.put("errMessage", "参数不能为空");
                return super.callbackFail(errMap);
            }
        } catch (Exception e) {
            Map<String, Object> errMap = new HashMap<>();
            errMap.put("errMessage", "系统异常");
            return super.callbackFail(errMap);
        }

    }
}
