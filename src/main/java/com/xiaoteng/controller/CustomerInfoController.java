package com.xiaoteng.controller;

import com.xiaoteng.authorization.annotation.Authorization;
import com.xiaoteng.authorization.annotation.CurrentUser;
import com.xiaoteng.core.controller.BaseController;
import com.xiaoteng.core.model.ApiResponse;
import com.xiaoteng.core.utils.ExcelUtil;
import com.xiaoteng.domin.ResponsePage;
import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.domin.UserResponse;
import com.xiaoteng.entity.AuthenticationBill;
import com.xiaoteng.entity.BaseUser;
import com.xiaoteng.entity.Contact;
import com.xiaoteng.entity.UserStatus;
import com.xiaoteng.service.AuthenticationBillService;
import com.xiaoteng.service.BaseUserService;
import com.xiaoteng.service.ContactService;
import com.xiaoteng.service.UserStatusService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

@CrossOrigin(maxAge=3600)
@Api(value = "业务客户接口",description = "业务客户信息相关接口")
@RestController
@RequestMapping("/useinfo")
public class CustomerInfoController extends BaseController {

    @Autowired
    BaseUserService baseUserService;
    @Autowired
    AuthenticationBillService authenticationBillService;
    @Autowired
    ContactService contactService;
    @Autowired
    UserStatusService userStatusService;

    @Authorization
    @ApiOperation(value="业务客户列表查询", notes="客户查询")
    @RequestMapping(value = "/list",method = RequestMethod.POST)
    public ApiResponse userList(@ApiParam(name="SearchPage实体",value="json格式",required=true)@RequestBody SearchPage searchPage,@CurrentUser UserResponse user) {

        try {
            if (searchPage != null) {
              // Page page=baseUserService.queryByproperties(searchPage);
                ResponsePage  responsePage=baseUserService.findUserListByproperties(searchPage,user.getId());
                return super.callbackSuccess(responsePage);
            } else {
                Map<String, Object> errMap = new HashMap<>();
                errMap.put("errMessage", "参数不能为空");
                return super.callbackFail(errMap);
            }
        } catch (Exception e) {
            Map<String, Object> errMap = new HashMap<>();
            errMap.put("errMessage", "系统异常");
            return super.callbackFail(errMap);
        }

    }
    @Authorization
    @ApiOperation(value="客户详情", notes="查询客户信息")
    @ApiImplicitParam(name = "userid", value = "用户id", required = true,dataType = "String",paramType = "path")
    @RequestMapping(value = "/useInfo/{userid}",method = RequestMethod.GET)
    public ApiResponse getUserDetail(@PathVariable String userid){

        try {
            if (userid!=null){
                Map<String,Object>userInfo=new HashMap<>();

                BaseUser baseUser=baseUserService.queryById(userid);
                AuthenticationBill authenticationBill=authenticationBillService.findByUserId(userid);
                List<Map<String,Object>> countContact=contactService.countAllByByUserId(userid);
                userInfo.put("baseInfo",baseUser);
                userInfo.put("creditInvestigation",authenticationBill);
                userInfo.put("countContact",countContact);

                return super.callbackSuccess(userInfo);
            }else {
                Map<String,Object>errMap=new HashMap<>();
                errMap.put("errMessage","电话号码不能为空");
                return super.callbackFail(errMap);
            }
        }catch (Exception e){
            Map<String,Object>errMap=new HashMap<>();
            errMap.put("errMessage","系统异常");
            return super.callbackFail(errMap);
        }

    }
    @Authorization
    @ApiOperation(value="客户通讯录", notes="查询客户通讯录信息")
    @ApiImplicitParam(name = "userid", value = "用户id", required = true,dataType = "String",paramType = "path")
    @RequestMapping(value = "/contactList/{userid}",method = RequestMethod.GET)
    public ApiResponse contactList(@PathVariable String userid){

        try {
            if (userid!=null){
                BaseUser baseUser=baseUserService.queryById(userid);
                List<Contact> contacts=new ArrayList<>();
                if (baseUser!=null){
                    contacts=contactService.findByUserId(userid);
                }

                return super.callbackSuccess(contacts);
            }else {
                Map<String,Object>errMap=new HashMap<>();
                errMap.put("errMessage","电话号码不能为空");
                return super.callbackFail(errMap);
            }
        }catch (Exception e){
            Map<String,Object>errMap=new HashMap<>();
            errMap.put("errMessage","系统异常");
            return super.callbackFail(errMap);
        }

    }
    @Authorization
    @ApiOperation(value="客户通讯录下载", notes="查询下载客户通讯录信息")
    @ApiImplicitParam(name = "userid", value = "用户id", required = true,dataType = "String",paramType = "path")
    @RequestMapping(value = "/downloadContactList/{userid}",produces = {"application/vnd.ms-excel;charset=UTF-8"},method = RequestMethod.GET)
    public ApiResponse downloadContactList(@PathVariable String userid,HttpServletResponse response){
        try {

            BaseUser baseUser=baseUserService.queryById(userid);
            List<Contact> contacts=new ArrayList<>();
            if (baseUser!=null){
                contacts=contactService.findByUserId(userid);

            }
            String fileName=baseUser!=null?baseUser.getUserName()+"通讯录"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()):" 通讯录"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

            List<Map<String,Object>> list=createExcelRecord(contacts);
        String columnNames[]={"id","用户id","姓名","电话号码"};//列名
        String keys[]    =     {"id","userid","givenName","phonenumber"};//map中的key
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            ExcelUtil.createWorkBook(list,keys,columnNames).write(os);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] content = os.toByteArray();
        InputStream is = new ByteArrayInputStream(content);
        // 设置response参数，可以打开下载页面
        response.reset();
        response.setContentType("application/octet-stream;charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename="+ new String((fileName + ".xls").getBytes("utf-8"), "ISO-8859-1"));
        ServletOutputStream out = response.getOutputStream();
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            bis = new BufferedInputStream(is);
            bos = new BufferedOutputStream(out);
            byte[] buff = new byte[2048];
            int bytesRead;
            // Simple read/write loop.
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
        } catch (final IOException e) {
            throw e;
        } finally {
            if (bis != null){
                bis.close();
            }

            if (bos != null){
                bos.close();
            }

        }
    }catch (Exception e){

    }

        return null;

    }

    private List<Map<String, Object>> createExcelRecord(List<Contact> contacts) {
        List<Map<String, Object>> listmap = new ArrayList<Map<String, Object>>(600);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("sheetName", "通讯录");
        listmap.add(map);
        for (int j = 0; j < contacts.size(); j++) {
            Contact contact=contacts.get(j);
            Map<String, Object> mapValue = new HashMap<String, Object>(4);
            mapValue.put("givenName", contact.getGivenName());
            mapValue.put("phonenumber",contact.getNumber());
            mapValue.put("id",contact.getId());
            mapValue.put("userid",contact.getUserId());
            listmap.add(mapValue);
        }
        return listmap;
    }
    @ApiOperation(value="更新客户状态", notes="修改客户状态")
    @ApiImplicitParams({
        @ApiImplicitParam (name = "userid", value = "用户id", required = true,dataType = "String",paramType = "path"),
        @ApiImplicitParam(name = "status", value = "用户状态", required = true,dataType = "String",paramType = "path")
    })

    @Authorization
    @RequestMapping(value = "/updateUserStatus/{userid}/{status}",method = RequestMethod.GET)
    public ApiResponse updateUserStatus(@PathVariable String userid,@PathVariable String status) {

        try {
            if ((userid != null||!userid.equals(""))&&(status!=null||!status.equals(""))) {
                // Page page=baseUserService.queryByproperties(searchPage);
                UserStatus userStatus=userStatusService.findByUserId(userid);
                if (userStatus!=null){
                    userStatusService.updateUserStatus(status,userid);
                    Map<String, Object> errMap = new HashMap<>();
                    errMap.put("userid",userid);
                    return super.callbackSuccess(errMap);
                }else{
                    Map<String, Object> errMap = new HashMap<>();
                    errMap.put("errMessage", "没有找到userid："+userid+"的数据");
                    return super.callbackFail(errMap);
                }

            } else {
                Map<String, Object> errMap = new HashMap<>();
                errMap.put("errMessage", "参数不能为空");
                return super.callbackFail(errMap);
            }
        } catch (Exception e) {
            Map<String, Object> errMap = new HashMap<>();
            errMap.put("errMessage", "系统异常");
            return super.callbackFail(errMap);
        }

    }
}
