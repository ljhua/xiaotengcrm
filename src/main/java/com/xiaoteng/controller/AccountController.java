package com.xiaoteng.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xiaoteng.api.authorization.Token;
import com.xiaoteng.api.authorization.jwt.JwtConfig;
import com.xiaoteng.api.authorization.jwt.JwtTokenBuilder;
import com.xiaoteng.authorization.annotation.Authorization;
import com.xiaoteng.authorization.annotation.CurrentUser;
import com.xiaoteng.authorization.manager.TokenManager;
import com.xiaoteng.core.controller.BaseController;
import com.xiaoteng.core.model.ApiResponse;
import com.xiaoteng.domin.UserResponse;
import com.xiaoteng.entity.User;
import com.xiaoteng.exception.ApplicationException;
import com.xiaoteng.exception.StatusCode;
import com.xiaoteng.exception.SystemError;
import com.xiaoteng.service.UserService;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Function:登录控制器. <br/>
 */
@CrossOrigin(maxAge=3600)
@RestController
@RequestMapping(value = "/api")
@Api(value = "用户登录管理",description = "用户登录管理接口")
public class AccountController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private JwtConfig jwtConfig;

    @Autowired
    private JwtTokenBuilder jwtTokenBuilder;

    @Autowired
    private TokenManager tokenManager;

    /**
     * 登录
     *
     * @return
     */
    @ApiOperation(value="用户登录", notes="用户登录接口")
    @RequestMapping(value = "/login", method = RequestMethod.GET)
   public ApiResponse login(@RequestParam(value = "mobile") String mobile, @RequestParam(value = "password") String password) throws Exception {
        //验证码校验

        //用户名密码验证
        User user = userService.auth(mobile, password);

        if (user != null) {
            Map map = new HashMap();
            map.put("mobile", user.getMobile());
            String subject = JwtTokenBuilder.buildSubject(map);

            String accessToken = jwtTokenBuilder.buildToken(subject, jwtConfig.getExpiresSecond(), jwtConfig.getBase64Secret());
            String refreshToken = jwtTokenBuilder.buildToken(subject, jwtConfig.getRefreshExpiresSecond(), jwtConfig.getRefreshBase64Secret());

            Token token = new Token();
            token.setAccess_token(accessToken);
            token.setRefresh_token(refreshToken);
            token.setToken_type("bearer");
            token.setExpires_in(jwtConfig.getExpiresSecond());

            //存储到redis
            tokenManager.createRelationship(mobile, accessToken);

            return super.callbackSuccess(token);
        } else {
            Map<String,Object> errMap=new HashMap<>();
            errMap.put(Integer.toString(SystemError.LOGIN_FAILED.getCode()),SystemError.LOGIN_FAILED.getMessage());
            return super.callbackFail(errMap);
        }
    }

    /**
     * 刷新token，获取新的token
     *
     * @param refresh_token
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/isAuthExpired", method = RequestMethod.GET)
    @ResponseBody
    public ApiResponse refreshToken(@RequestParam(value = "refresh_token") String refresh_token) throws Exception {

        //验证token
        Claims claims = jwtTokenBuilder.decodeToken(refresh_token, jwtConfig.getRefreshBase64Secret());
        if (claims != null) {
            //如果token验证成功，返回新的token
            String subject = claims.getSubject();//用户信息
            String accessToken = jwtTokenBuilder.buildToken(subject, jwtConfig.getExpiresSecond(), jwtConfig.getBase64Secret());
            String refreshToken = jwtTokenBuilder.buildToken(subject, jwtConfig.getRefreshExpiresSecond(), jwtConfig.getRefreshBase64Secret());

            Token token = new Token();
            token.setAccess_token(accessToken);
            token.setRefresh_token(refreshToken);
            token.setToken_type("bearer");
            token.setExpires_in(jwtConfig.getExpiresSecond());
            JSONObject currentUser=JSON.parseObject(subject);
            //存储到redis
            tokenManager.createRelationship(currentUser.getString("mobile"), accessToken);

            return super.callbackSuccess(token);
        } else {
            //throw new ApplicationException(StatusCode.UNAUTHORIZED.getCode(), "invalid refresh token");
            Map<String,Object> errMap=new HashMap<>();
            errMap.put(Integer.toString(StatusCode.UNAUTHORIZED.getCode()),"invalid refresh token");
            return super.callbackFail(errMap);
        }
    }

    /**
     * 注销
     *
     * @return
     */
    @ApiOperation(value="用户登录", notes="用户注销接口")
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public Integer logout(@RequestHeader(name = "Authorization") String token) {
        //TODO 操蛋的JWT不能从服务端destroy token， logout目前只能在客户端的cookie 或 localStorage/sessionStorage  remove token
        //TODO 准备用jwt生成永久的token，再结合redis来实现Logout。具体是把token的生命周期交给redis来管理，jwt只负责生成token

        try {
            //多端登录，会有多个同一用户名但token不一样的键值对在redis中存在，所以只能通过token删除
//        tokenManager.delRelationshipByKey(user.getUsername());
            tokenManager.delRelationshipByToken(token);//注销成功
            return 1;
        } catch (Exception e) {
            return -1;
        }
    }

    /**
     * 测试token验证
     *
     * @return
     */
    @Authorization
    @RequestMapping(value = "/user/get", method = RequestMethod.GET)
    @ResponseBody
    public User get(@CurrentUser UserResponse userResponse) {
        return userService.getByMobile(userResponse.getMobile());
    }
}
