package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.EmergencyPerson;

public interface EmergencyPersonService extends BaseService<EmergencyPerson> {
    public EmergencyPerson findByUserId(String userId);
}
