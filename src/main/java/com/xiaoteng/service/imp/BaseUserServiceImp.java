package com.xiaoteng.service.imp;


import com.xiaoteng.core.utils.DateFormatUtils;
import com.xiaoteng.domin.ResponsePage;
import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.BaseUser;
import com.xiaoteng.jpa.BaseUserRepository;
import com.xiaoteng.service.BaseUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class BaseUserServiceImp implements BaseUserService {

    @Autowired
    BaseUserRepository baseUserRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;


    @Override
    public void delete(String id) {

    }

    @Override
    public BaseUser queryById(String id) {
        return baseUserRepository.findOne(id);
    }

    @Override
    public List<BaseUser> queryAll() {
        return null;
    }

    @Override
    public Page<BaseUser> queryByproperties(SearchPage searchPage) {
        Pageable pageable=new PageRequest(searchPage.getPage(), searchPage.getSize());  //分页信息
        Specification<BaseUser> spec = new Specification<BaseUser>() {        //查询条件构造
             @Override
             public Predicate toPredicate(Root<BaseUser> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                 List<Predicate> predicates = new ArrayList<>();

                   String param=searchPage.getCondition();
                    String sourceParam=searchPage.getSource();
                    String registerStartParam=searchPage.getRegisterStart();
                    String registerEndParam=searchPage.getRegisterEnd();
                     Path<String> userName = root.get("userName");
                     Path<String> telphoneNumber = root.get("telphoneNumber");
                     Path<String> idCard = root.get("idCard");
                     Path<String> source = root.get("source");
                     Path<Date>  createDate= root.get("createDate");

                     if (!StringUtils.isNotBlank(param)){
                         Predicate pname = cb.like(userName, "%"+param+"%");
                         Predicate pphone = cb.like(telphoneNumber, "%"+param+"%");
                         Predicate pcard = cb.like(idCard, "%"+param+"%");
                         Predicate p = cb.or(pname, pphone,pcard);
                         predicates.add(p);
                     }
                 if (!StringUtils.isNotBlank(sourceParam)){
                     Predicate psource = cb.equal(source, sourceParam);

                     predicates.add(psource);
                 }
                    if (!StringUtils.isNotBlank(registerStartParam)){
                        Date createStart= DateFormatUtils.StringToDate(registerStartParam,"yyyy-MM-dd HH:mm:ss");
                        predicates.add(cb.greaterThanOrEqualTo(createDate,createStart));
                    }
                     if (!StringUtils.isNotBlank(registerEndParam)){
                         Date createEnd= DateFormatUtils.StringToDate(registerStartParam,"yyyy-MM-dd HH:mm:ss");
                         predicates.add(cb.lessThanOrEqualTo(createDate,createEnd));
                     }


                     return cb.and(predicates.toArray(new Predicate[predicates.size()]));
              }

         };

    return baseUserRepository.findAll(spec, pageable);
    }

    @Override
    public BaseUser save(BaseUser baseUser) {
        idEnsure(baseUser);
        createOrUpdateDateEnsure(baseUser);
        return baseUserRepository.save(baseUser);
    }

    @Override
    public ResponsePage findUserListByproperties(SearchPage searchPage,String id) {
       StringBuffer sql=new StringBuffer("SELECT t1.id as userid,t1.username as username,t1.telphonenumber as userphone,\n" +
               "       t1.idcard as useridcard,t1.source  as source,DATE_FORMAT(t1.create_date,'%Y-%m-%d %h:%i:%s') as registerdate,\n" +
               "       t2.userstatus from base_user t1\n" +
               "LEFT JOIN user_status t2 on t2.userid=t1.id\n" +
               "LEFT JOIN sys_user t3 on t3.id=t1.source \n"+
               "WHERE  1=1 ");
        StringBuffer countSql=new StringBuffer("SELECT COUNT(t1.id) allSize from base_user t1\n" +
                " LEFT JOIN user_status t2 on t2.userid=t1.id\n" +
                "LEFT JOIN sys_user t3 on t3.id=t1.source \n"+
                " WHERE  1=1 ");
        StringBuffer whereSql=new StringBuffer();
        ResponsePage responsePage=new ResponsePage();
        int page=searchPage.getPage();
        int size=searchPage.getSize();
        if (size==0){
            size=1;
        }
        if (!id.equals("1")){

            whereSql.append(" and t3.id='"+id+"'");
        }
         String condition=searchPage.getCondition();;//电话号码、身份证、用户名
        if (StringUtils.isNotBlank(condition)){
            whereSql.append(" AND (t1.username LIKE '%"+condition+"%' OR t1.telphonenumber LIKE '%"+condition+"%' OR t1.idcard LIKE '%"+condition+"%')");
        }
         String source=searchPage.getSource();//用户来源
        if (StringUtils.isNotBlank(source)){
            whereSql.append(" AND t1.source ='"+source+"'");
        }
        String userStatus=searchPage.getUserStatus();//用户状态
        if (StringUtils.isNotBlank(userStatus)){
            whereSql.append(" AND t2.userstatus ='"+userStatus+"'");
        }
         String registerStart=searchPage.getRegisterStart();//注册开始时间
        if (StringUtils.isNotBlank(registerStart)){
            whereSql.append(" AND t1.create_date >='"+registerStart+"'");
        }
         String registerEnd=searchPage.getRegisterEnd();//注册结束时间
        if (StringUtils.isNotBlank(registerEnd)){
            whereSql.append(" AND t1.create_date <='"+registerEnd+"'");
        }

        responsePage.setNumber(page);
        responsePage.setPageSize(size);


       Map<String ,Object>allSizeMap=jdbcTemplate.queryForMap(countSql.append(whereSql).toString());
       int allSize=Integer.parseInt(allSizeMap.get("allSize").toString());
       int startIndex=(page-1)*size;
        responsePage.setTotal(allSize);
        whereSql.append("order by t1.create_date desc");
        whereSql.append(" LIMIT "+startIndex+","+size);
      List<Map<String,Object>>  mapList=jdbcTemplate.queryForList(sql.append(whereSql).toString());
        responsePage.setContent(mapList);

        return responsePage;
    }
}
