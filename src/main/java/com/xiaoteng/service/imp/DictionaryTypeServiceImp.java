package com.xiaoteng.service.imp;


import com.xiaoteng.entity.DictionaryType;
import com.xiaoteng.service.DictionaryTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * DictionaryType Service实现类
 * @create 2018-11-23 17:45:00
 */
@Service
public class DictionaryTypeServiceImp implements DictionaryTypeService {


    @Override
    public DictionaryType save(DictionaryType request) {
        return null;
    }

    @Override
    public DictionaryType update(DictionaryType request) {
        return null;
    }

    @Override
    public Integer del(Long id) {
        return null;
    }

    @Override
    public DictionaryType get(Long id) {
        return null;
    }

    @Override
    public List<DictionaryType> getDictionaryTypes() {
        return null;
    }
}
