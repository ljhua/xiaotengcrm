package com.xiaoteng.service.imp;


import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.AuthenticationInfo;
import com.xiaoteng.jpa.AuthenticationInfoRepository;
import com.xiaoteng.entity.BaseUser;
import com.xiaoteng.service.AuthenticationInfoService;
import com.xiaoteng.service.BaseUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthenticationInfoServiceImp implements AuthenticationInfoService {

    @Autowired
    AuthenticationInfoRepository authenticationInfoRepository;
    @Autowired
    BaseUserService baseUserService;
    @Override
    public Page queryByproperties(SearchPage searchPage) {
        return null;
    }

    @Override
    public void delete(String id) {

    }

    @Override
    public AuthenticationInfo queryById(String id) {
        return null;
    }

    @Override
    public List<AuthenticationInfo> queryAll() {
        return null;
    }

    @Override
    public AuthenticationInfo save(AuthenticationInfo authenticationInfo) {
        return null;
    }
}
