package com.xiaoteng.service.imp;


import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.LiveBody;
import com.xiaoteng.jpa.LiveBodyRepository;
import com.xiaoteng.service.LiveBodyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LiveBodyServiceImp implements LiveBodyService {

    @Autowired
    LiveBodyRepository liveBodyRepository;

    @Override
    public void delete(String id) {

    }
    @Override
    public Page queryByproperties(SearchPage searchPage) {
        return null;
    }
    @Override
    public LiveBody queryById(String id) {
        return null;
    }

    @Override
    public List<LiveBody> queryAll() {
        return null;
    }

    @Override
    public LiveBody save(LiveBody liveBody) {
        idEnsure(liveBody);
        createOrUpdateDateEnsure(liveBody);
        return liveBodyRepository.save(liveBody);
    }
    public  LiveBody findByUserId(String userid){
        return  liveBodyRepository.findByUserId(userid);
    }
}
