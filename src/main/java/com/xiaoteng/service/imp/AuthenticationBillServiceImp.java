package com.xiaoteng.service.imp;


import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.AuthenticationBill;
import com.xiaoteng.jpa.AuthenticationBillRepository;
import com.xiaoteng.service.AuthenticationBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthenticationBillServiceImp  implements AuthenticationBillService {

    @Autowired
    AuthenticationBillRepository authenticationBillRepository;

    @Override
    public void delete(String id) {

    }

    @Override
    public AuthenticationBill queryById(String id) {
        return null;
    }

    @Override
    public List<AuthenticationBill> queryAll() {
        return null;
    }

    @Override
    public AuthenticationBill save(AuthenticationBill authenticationBill) {
        idEnsure(authenticationBill);
        createOrUpdateDateEnsure(authenticationBill);
        return authenticationBillRepository.save(authenticationBill);
    }
    @Override
    public Page queryByproperties(SearchPage searchPage) {
        return null;
    }
    @Override
    public AuthenticationBill findByUserId(String userId) {
        return authenticationBillRepository.findByUserId(userId);
    }
}
