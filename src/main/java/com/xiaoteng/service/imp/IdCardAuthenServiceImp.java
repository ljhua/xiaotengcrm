package com.xiaoteng.service.imp;


import com.xiaoteng.core.utils.TimeUtils;
import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.IdCardAuthen;
import com.xiaoteng.jpa.IdCardAuthenRepository;
import com.xiaoteng.service.BaseUserService;
import com.xiaoteng.service.IdCardAuthenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;

@Service
public class IdCardAuthenServiceImp implements IdCardAuthenService {

    @Autowired
    BaseUserService baseUserService;
    //String tempFilePath="F:\\workspace\\IdeaProjects\\temp";
    String tempFilePath="/opt/file";

    @Autowired
    IdCardAuthenRepository idCardAuthenRepository;

    @Override
    public void delete(String id) {

    }

    @Override
    public IdCardAuthen queryById(String id) {
        return null;
    }

    @Override
    public List<IdCardAuthen> queryAll() {
        return null;
    }

    @Transactional
    @Override
    public IdCardAuthen save(IdCardAuthen idCardAuthen) {
        idEnsure(idCardAuthen);
        createOrUpdateDateEnsure(idCardAuthen);
        IdCardAuthen idCardAuthen1=idCardAuthenRepository.save(idCardAuthen);
        //baseUserService.updateBaseUser(idCardAuthen.getUserId(),idCardAuthen.getNumber(),idCardAuthen.getName());
        return idCardAuthen1;
    }
    @Override
    public Page queryByproperties(SearchPage searchPage) {
        return null;
    }
    @Override
    public IdCardAuthen findByUserId(String userid) {
        return idCardAuthenRepository.findByUserId(userid);
    }



}
