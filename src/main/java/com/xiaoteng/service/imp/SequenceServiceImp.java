package com.xiaoteng.service.imp;

import com.xiaoteng.service.SequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class SequenceServiceImp implements SequenceService {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public String getSequence(String name) {

        return jdbcTemplate.queryForObject("SELECT CURRVAL('"+name+"')",String.class);
    }

    @Override
    public String getNextSequence(String name) {
        return jdbcTemplate.queryForObject("SELECT NEXTVAL('"+name+"')",String.class);
    }

    @Override
    public String setSequence(String name) {
        return jdbcTemplate.queryForObject("SELECT SETVAL('"+name+"',100000000)",String.class);
    }
}
