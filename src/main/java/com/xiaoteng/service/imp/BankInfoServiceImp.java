package com.xiaoteng.service.imp;


import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.BankInfo;
import com.xiaoteng.jpa.BankInfoRepository;
import com.xiaoteng.service.BankInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankInfoServiceImp implements BankInfoService {
    @Autowired
    BankInfoRepository bankInfoRepository;
    @Override
    public void delete(String id) {

    }

    @Override
    public BankInfo queryById(String id) {
        return null;
    }

    @Override
    public List<BankInfo> queryAll() {
        return null;
    }

    @Override
    public BankInfo save(BankInfo bankInfo) {
        idEnsure(bankInfo);
        createOrUpdateDateEnsure(bankInfo);
        return bankInfoRepository.save(bankInfo);
    }

    @Override
    public List<BankInfo> findByUserId(String userId) {

        return   bankInfoRepository.findByUserId(userId);
    }
    @Override
    public Page queryByproperties(SearchPage searchPage) {
        return null;
    }
}
