package com.xiaoteng.service.imp;

import com.xiaoteng.entity.Dictionary;
import com.xiaoteng.service.DictionaryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Dictionary Service实现类
 * @create 2018-11-23 17:45:00
 */
@Service
public class DictionaryServiceImp implements DictionaryService {


    @Override
    public Dictionary save(Dictionary request) {
        return null;
    }

    @Override
    public Dictionary update(Dictionary request) {
        return null;
    }

    @Override
    public Integer del(Long id) {
        return null;
    }

    @Override
    public Dictionary get(Long id) {
        return null;
    }

    @Override
    public List<Dictionary> getDictionarys(String type) {
        return null;
    }
}
