package com.xiaoteng.service.imp;

import com.xiaoteng.service.UserRoleService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Role Service实现类
 *
 * @create 2018-11-23 17:45:00
 */
@Service
public class UserRoleServiceImp implements UserRoleService {

    @Override
    public Integer deleteUserRole(Long userId, Long roleId) {
        return null;
    }

    @Override
    public Integer changeUserRole(Long userId, List<Long> targetRoles) {
        return null;
    }

    @Override
    public Integer deleteUserRoleByuserId(Long userId) {
        return null;
    }
}
