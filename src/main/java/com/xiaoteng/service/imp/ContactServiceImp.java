package com.xiaoteng.service.imp;


import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.Contact;
import com.xiaoteng.jpa.ContactRepository;
import com.xiaoteng.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ContactServiceImp implements ContactService {
    @Autowired
    ContactRepository contactRepository;
    @Override
    public void delete(String id) {

    }

    @Override
    public Contact queryById(String id) {
        return null;
    }

    @Override
    public List<Contact> queryAll() {
        return null;
    }

    @Override
    public Contact save(Contact contact) {
        idEnsure(contact);
        createOrUpdateDateEnsure(contact);
        return contactRepository.save(contact);
    }

    @Override
    public List<Map<String,Object>> countAllByByUserId(String userId) {
        return contactRepository.countByUserId(userId);
    }

    @Override
    public List<Contact> findByUserId(String userId) {
        return contactRepository.findByUserId(userId);
    }
    @Override
    public Page queryByproperties(SearchPage searchPage) {
        return null;
    }
}
