package com.xiaoteng.service.imp;


import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.EmergencyPerson;
import com.xiaoteng.jpa.EmergencyPersonRepository;
import com.xiaoteng.service.EmergencyPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmergencyPersonServiceImp implements EmergencyPersonService {

    @Autowired
    EmergencyPersonRepository emergencyPersonRepository;

    @Override
    public void delete(String id) {

    }
    @Override
    public Page queryByproperties(SearchPage searchPage) {
        return null;
    }
    @Override
    public EmergencyPerson queryById(String id) {
        return null;
    }

    @Override
    public List<EmergencyPerson> queryAll() {
        return null;
    }

    @Override
    public EmergencyPerson save(EmergencyPerson emergencyPerson) {
        idEnsure(emergencyPerson);
        createOrUpdateDateEnsure(emergencyPerson);

        return emergencyPersonRepository.save(emergencyPerson);
    }

    @Override
    public EmergencyPerson findByUserId(String userId) {
        return emergencyPersonRepository.findByUserId(userId);
    }
}
