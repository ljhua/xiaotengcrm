package com.xiaoteng.service.imp;


import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.AuthenCallback;
import com.xiaoteng.jpa.AuthenCallbackRepository;
import com.xiaoteng.service.AuthenCallbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;

@Service
public class AuthenCallbackServiceImp implements AuthenCallbackService {
    @Autowired
    AuthenCallbackRepository authenCallbackRepository;

    @Override
    public void delete(String id) {

    }

    @Override
    public AuthenCallback queryById(String id) {
        return null;
    }

    @Override
    public List<AuthenCallback> queryAll() {
        return null;
    }

    @Override
    public Page queryByproperties(SearchPage searchPage) {
        return null;
    }

    @Override
    public AuthenCallback save(AuthenCallback authenCallback) {
        idEnsure(authenCallback);
        createOrUpdateDateEnsure(authenCallback);
        return authenCallbackRepository.save(authenCallback);
    }


    @Override
    public AuthenCallback findByUserId(String userid) {
        return authenCallbackRepository.findByUserId(userid);
    }

    @PostConstruct
    public void init(){
            System.out.printf("init AuthenCallbackServiceImp bean\n");
    }
    @PreDestroy
    public void destroy(){
        System.out.printf("destroy  AuthenCallbackServiceImp bean\n");
    }
}
