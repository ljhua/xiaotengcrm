package com.xiaoteng.service.imp;


import com.xiaoteng.domin.ResponsePage;
import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.domin.SourceResponse;
import com.xiaoteng.domin.UserResponse;
import com.xiaoteng.entity.User;
import com.xiaoteng.exception.ApplicationException;
import com.xiaoteng.exception.StatusCode;
import com.xiaoteng.exception.SystemError;
import com.xiaoteng.jpa.UserModelBeanRepository;
import com.xiaoteng.service.UserService;

import com.xiaoteng.utils.MD5Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *  * @create 2018-11-23 17:45:00
 */
@Service
public class UserServiceImp implements UserService {

    @Autowired
    UserModelBeanRepository userModelBeanRepository;
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Override
    public User save(User request) {
        idEnsure(request);
        createOrUpdateDateEnsure(request);
        if (request.getPassword()==null||"".equals(request.getPassword())){
            request.setPassword("123456");
        }

        request.setPassword(MD5Utils.getStringMD5(request.getPassword()));
        return userModelBeanRepository.save(request);
    }
   //更新
   @Transactional
    @Override
    public void update(User request) {
        updateDateEnsure(request);

        userModelBeanRepository.update(request.getStatus(),request.getMobile(),request.getName(),request.getUpdateUser(),request.getUpdateTime(),request.getIsDeduction(),request.getDeductionRatio(),request.getAddress(),request.getPromoters(),request.getId());
    }
    //扣点接口
    @Transactional
    @Override
    public void updateUserDeduction(User request) {
        updateDateEnsure(request);
        userModelBeanRepository.updateUserDeduction(request.getUpdateUser(), request.getUpdateTime(), request.getIsDeduction(),request.getDeductionRatio(),request.getId());
    }

    @Override
    public void del(String id) {
      userModelBeanRepository.delete(id);
    }


    @Override
    public SourceResponse get(String id) {
        SourceResponse sourceResponse=new SourceResponse();
        User user=userModelBeanRepository.findOne(id);
        if (user!=null){
            BeanUtils.copyProperties(user,sourceResponse);
        }
        return sourceResponse;
    }

    @Override
    public User findUserByUserName(String username) {
        return null;
    }

    @Override
    public User getUser(String username) {
        return null;
    }

    @Override
    public List<User> getUsers(User request) {
        return null;
    }

    @Override
    public Integer modifyPassword(Long userId, String originalPassword, String newPassword) {
        return null;
    }

    @Override
    public User auth(String username, String password) {
        List<User>users=userModelBeanRepository.findByMobile(username);
        User existing=users!=null&&users.size()>0?users.get(0):null;
        if (existing != null) {
            String encryData = MD5Utils.getStringMD5(password);
            if (existing.getPassword().equals(encryData)) {//验证密码是否正确
                return existing;
            } else {
                //密码错误
                //throw new ApplicationException(SystemError.LOGIN_FAILED.getCode(), SystemError.LOGIN_FAILED.getMessage());
                return  null;
            }
        } else {
            //用户不存在
         return  null;  // throw new ApplicationException(StatusCode.NOT_FOUND.getCode(), StatusCode.NOT_FOUND.getMessage());
        }
    }

    @Override
    public User getByMobile(String mobile) {
        List<User>users=userModelBeanRepository.findByMobile(mobile);
        User existing=users!=null?users.get(0):null;
        return existing;
    }

    @Override
    public UserResponse getFindByMobile(String mobile) {
        List<User>users=userModelBeanRepository.findByMobile(mobile);
        User existing=users!=null?users.get(0):null;
       UserResponse userResponse= new UserResponse();
       if (existing!=null){
         BeanUtils.copyProperties(existing,userResponse);
       }
        return userResponse;
    }

    @Override
    public ResponsePage findUserListByproperties(SearchPage searchPage) {
        StringBuffer sql=new StringBuffer("SELECT t1.id,t1.name,DATE_FORMAT(t1.create_time,'%Y-%m-%d %h:%i:%s') as createTime, " +
                "DATE_FORMAT(t1.update_time,'%Y-%m-%d %h:%i:%s') as updateTime,t1.status," +
                "t1.is_deduction  as isDeduction,t1.deduction_ratio as deductionRatio,t1.promoters from sys_user t1" +
                " WHERE  1=1 ");
        StringBuffer countSql=new StringBuffer("SELECT COUNT(t1.id) allSize from sys_user t1\n" +
                " WHERE  1=1 ");
        StringBuffer whereSql=new StringBuffer();
        ResponsePage responsePage=new ResponsePage();
        int page=searchPage.getPage();
        int size=searchPage.getSize();
        if (size==0){
            size=1;
        }
        String condition=searchPage.getCondition();;//电话号码、身份证、用户名
        if (StringUtils.isNotBlank(condition)){
            whereSql.append(" AND (t1.name LIKE '%"+condition+"%' OR t1.mobile LIKE '%"+condition+"%')");
        }

        String userStatus=searchPage.getUserStatus();//用户状态
        if (StringUtils.isNotBlank(userStatus)){
            whereSql.append(" AND t1.status ="+userStatus+" ");
        }
        String registerStart=searchPage.getRegisterStart();//注册开始时间
        if (StringUtils.isNotBlank(registerStart)){
            whereSql.append(" AND t1.create_time >='"+registerStart+"' ");
        }
        String registerEnd=searchPage.getRegisterEnd();//注册结束时间
        if (StringUtils.isNotBlank(registerEnd)){
            whereSql.append(" AND t1.create_time <='"+registerEnd+"' ");
        }

        responsePage.setNumber(page);
        responsePage.setPageSize(size);

        Map<String ,Object> allSizeMap=jdbcTemplate.queryForMap(countSql.append(whereSql).toString());
        int allSize=Integer.parseInt(allSizeMap.get("allSize").toString());
        int startIndex=(page-1)*size;
        responsePage.setTotal(allSize);
        whereSql.append(" order by t1.create_time desc");
        whereSql.append(" LIMIT "+startIndex+","+size);
        List<Map<String,Object>>  mapList=jdbcTemplate.queryForList(sql.append(whereSql).toString());
        responsePage.setContent(mapList);

        return responsePage;
    }

    @Override
    public ResponsePage statisticalAnalysisList(SearchPage searchPage) {
        StringBuffer sql=new StringBuffer("select t2.source,\n" +
                "(SELECT t1.name FROM sys_user t1 WHERE t1.id=t2.source) as name,\n" +
                "COUNT(t2.source) as registerCount\n" +
                " from base_user t2");

        StringBuffer countSql=new StringBuffer("select COUNT(t2.source) allSize\n" +
                " from base_user t2 ");
        StringBuffer whereSql=new StringBuffer(" WHERE 1=1 ");
        ResponsePage responsePage=new ResponsePage();
        int page=searchPage.getPage();
        int size=searchPage.getSize();
        if (size==0){
            size=1;
        }

        String registerStart=searchPage.getRegisterStart();//注册开始时间
        if (StringUtils.isNotBlank(registerStart)){
            whereSql.append(" AND t1.create_time >='"+registerStart+"' ");
        }
        String registerEnd=searchPage.getRegisterEnd();//注册结束时间
        if (StringUtils.isNotBlank(registerEnd)){
            whereSql.append(" AND t1.create_time <='"+registerEnd+"' ");
        }
        whereSql.append(" GROUP BY t2.source");
        responsePage.setNumber(page);
        responsePage.setPageSize(size);

        Map<String ,Object> allSizeMap=jdbcTemplate.queryForMap(countSql.append(whereSql).toString());
        int allSize=Integer.parseInt(allSizeMap.get("allSize").toString());
        int startIndex=(page-1)*size;
        responsePage.setTotal(allSize);
        whereSql.append(" LIMIT "+startIndex+","+size);
        List<Map<String,Object>>  mapList=jdbcTemplate.queryForList(sql.append(whereSql).toString());
        responsePage.setContent(mapList);

        return responsePage;
    }

    @Override
    public List<User> getAll() {
        return null;
    }

}
