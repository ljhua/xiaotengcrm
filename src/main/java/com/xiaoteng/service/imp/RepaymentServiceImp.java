package com.xiaoteng.service.imp;


import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.Repayment;
import com.xiaoteng.jpa.RepaymentRepository;
import com.xiaoteng.service.RepaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepaymentServiceImp implements RepaymentService {

    @Autowired
    public RepaymentRepository repaymentRepository;

    @Override
    public List<Repayment> findByUserId(String userId) {
        return repaymentRepository.findByUserId(userId);
    }

    @Override
    public void delete(String id) {

    }
    @Override
    public Page queryByproperties(SearchPage searchPage) {
        return null;
    }
    @Override
    public Repayment queryById(String id) {
        return null;
    }

    @Override
    public List<Repayment> queryAll() {
        return null;
    }

    @Override
    public Repayment save(Repayment repayment) {
        idEnsure(repayment);
        createOrUpdateDateEnsure(repayment);
        return repaymentRepository.save(repayment);
    }
}
