package com.xiaoteng.service.imp;


import com.xiaoteng.entity.Role;
import com.xiaoteng.service.RoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Role Service实现类
 *
 * @create 2018-11-23 17:45:00
 */
@Service
public class RoleServiceImp  implements RoleService {
    @Override
    public Role save(Role request) {
        return null;
    }

    @Override
    public Role update(Role request) {
        return null;
    }

    @Override
    public Integer del(Long id) {
        return null;
    }

    @Override
    public Role get(Long id) {
        return null;
    }

    @Override
    public List<Role> getRoles() {
        return null;
    }

    @Override
    public List<Role> getRoles(String username) {
        return null;
    }

    @Override
    public List<Role> getRolesByUserId(Long userId) {
        return null;
    }
}
