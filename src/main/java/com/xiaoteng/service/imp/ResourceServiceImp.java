package com.xiaoteng.service.imp;


import com.xiaoteng.entity.Resource;
import com.xiaoteng.service.ResourceService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Resource Service实现类
 * @create 2018-11-23 17:45:00
 */
@Service
public class ResourceServiceImp implements ResourceService {

  @Override
  public Resource save(Resource request) {
    return null;
  }

  @Override
  public Resource update(Resource request) {
    return null;
  }

  @Override
  public Integer del(Long id) {
    return null;
  }

  @Override
  public Resource get(Long id) {
    return null;
  }

  @Override
  public List<Resource> getResources() {
    return null;
  }

  @Override
  public List<Resource> getResources(Long roleId) {
    return null;
  }

  @Override
  public List<Resource> getResources(Long parentId, String type) {
    return null;
  }

  @Override
  public List<Resource> getResourceByUsername(String username, String type) {
    return null;
  }
}
