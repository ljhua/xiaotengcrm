package com.xiaoteng.service.imp;


import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.service.CommonJdbcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class CommonJdbcServiceImp implements CommonJdbcService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void insertAppDownload(String ip) {
    String sql="insert  into apprecord(id, createtime,phoneversion,ip) values(?,?,?,?)";
        jdbcTemplate.update(sql, UUID.randomUUID().toString().replace("-", ""),new Date(),"",ip);
    }

    @Override
    public void insertLinkedRecord(String ip) {
        String sql="insert  into linkedrecord(id, createtime,phoneversion,ip) values(?,?,?,?)";
        jdbcTemplate.update(sql, UUID.randomUUID().toString().replace("-", ""),new Date(),"",ip);

    }
}
