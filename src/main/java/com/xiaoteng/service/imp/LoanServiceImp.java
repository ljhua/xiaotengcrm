package com.xiaoteng.service.imp;


import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.Loan;
import com.xiaoteng.jpa.LoanRepository;
import com.xiaoteng.service.LoanService;
import com.xiaoteng.service.SequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LoanServiceImp implements LoanService {

    @Autowired
    public SequenceService sequenceService;
    @Autowired
    public LoanRepository loanRepository;

    @Override
    public List<Loan> findByUserId(String userId) {
        return loanRepository.findByUserId(userId);
    }

    @Override
    public void delete(String id) {

    }
    @Override
    public Page queryByproperties(SearchPage searchPage) {
        return null;
    }
    @Override
    public Loan queryById(String id) {
        return null;
    }

    @Override
    public List<Loan> queryAll() {
        return null;
    }

    @Override
    public Loan save(Loan loan) {
        idEnsure(loan);
        createOrUpdateDateEnsure(loan);
        String sequence=sequenceService.getNextSequence("loanSequence");
        loan.setLoanCode(sequence);
        return loanRepository.save(loan);
    }
}
