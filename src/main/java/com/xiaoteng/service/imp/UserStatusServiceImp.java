package com.xiaoteng.service.imp;


import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.AuthenCallback;
import com.xiaoteng.entity.UserStatus;
import com.xiaoteng.jpa.AuthenCallbackRepository;
import com.xiaoteng.jpa.UserStatusRepository;
import com.xiaoteng.service.AuthenCallbackService;
import com.xiaoteng.service.UserStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;

@Service
public class UserStatusServiceImp implements UserStatusService {
    @Autowired
    UserStatusRepository userStatusRepository;


    @PostConstruct
    public void init(){
            System.out.printf("init AuthenCallbackServiceImp bean\n");
    }
    @PreDestroy
    public void destroy(){
        System.out.printf("destroy  AuthenCallbackServiceImp bean\n");
    }

    @Override
    public UserStatus findByUserId(String userid) {
        return userStatusRepository.findByUserId(userid);
    }

    @Transactional
    @Override
    public void updateUserStatus(String userstatus, String userid) {
        userStatusRepository.updateUserStatus(userstatus,userid);
    }
}
