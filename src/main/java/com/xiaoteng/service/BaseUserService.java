package com.xiaoteng.service;

import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.domin.ResponsePage;
import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.entity.BaseUser;

import java.util.Map;

public interface BaseUserService  extends BaseService<BaseUser> {

    ResponsePage findUserListByproperties(SearchPage searchPage,String id );

}
