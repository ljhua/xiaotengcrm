package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.Repayment;

import java.util.List;


public interface RepaymentService  extends BaseService<Repayment> {

    public List<Repayment> findByUserId(String userId);
}
