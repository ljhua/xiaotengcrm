package com.xiaoteng.service;


import java.util.List;

/**
 * Resource Service接口
 */
public interface RoleResourceService {
    public Integer changeRolePermission(Long roleId, List<Long> targetResources);
}
