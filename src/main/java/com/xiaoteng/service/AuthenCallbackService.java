package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.AuthenCallback;

public interface AuthenCallbackService extends BaseService<AuthenCallback> {
    public AuthenCallback findByUserId(String userid);
}
