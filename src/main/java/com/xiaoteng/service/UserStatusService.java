package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.UserStatus;


public interface UserStatusService  {
    public UserStatus findByUserId(String userid);
    public void updateUserStatus(String userstatus,String userid);
}
