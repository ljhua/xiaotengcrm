package com.xiaoteng.service;

public interface SequenceService {
    public String getSequence(String name);//查询当前name对应的当前序列号
    public String getNextSequence(String name);//查询下一个name对应的当前序列号
    public String setSequence(String name);//重置name对应的序列号
}
