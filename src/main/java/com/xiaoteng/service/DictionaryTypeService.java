package com.xiaoteng.service;

import com.xiaoteng.entity.DictionaryType;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * DictionaryType Service接口
 */
public interface DictionaryTypeService {

    @Transactional
    public DictionaryType save(DictionaryType request);

    @Transactional
    public DictionaryType update(DictionaryType request);

    @Transactional
    public Integer del(Long id);

    public DictionaryType get(Long id);

    public List<DictionaryType> getDictionaryTypes();

}
