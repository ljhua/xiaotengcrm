package com.xiaoteng.service;


import com.xiaoteng.entity.Role;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Role Service接口
 */
public interface RoleService {

    @Transactional
    public Role save(Role request);

    @Transactional
    public Role update(Role request);

    @Transactional
    public Integer del(Long id);

    public Role get(Long id);

    public List<Role> getRoles();

    public List<Role> getRoles(String username);
    public List<Role> getRolesByUserId(Long userId);
}
