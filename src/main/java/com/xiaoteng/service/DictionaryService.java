package com.xiaoteng.service;

import com.xiaoteng.entity.Dictionary;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Dictionary Service接口
 */
public interface DictionaryService {

    @Transactional
    public Dictionary save(Dictionary request);

    @Transactional
    public Dictionary update(Dictionary request);

    @Transactional
    public Integer del(Long id);

    public Dictionary get(Long id);

    public List<Dictionary> getDictionarys(String type);

}
