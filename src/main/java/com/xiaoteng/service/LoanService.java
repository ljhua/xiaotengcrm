package com.xiaoteng.service;

import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.Loan;

import java.util.List;

public interface LoanService  extends BaseService<Loan>{
   public List<Loan> findByUserId(String userId);
}
