package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.AuthenticationTask;

public interface AuthenticationTaskService   extends BaseService<AuthenticationTask> {
}
