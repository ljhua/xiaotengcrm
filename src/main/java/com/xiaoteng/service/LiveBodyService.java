package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.LiveBody;

public interface LiveBodyService extends BaseService<LiveBody> {
    public  LiveBody findByUserId(String userid);
}
