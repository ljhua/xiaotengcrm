package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.AuthenticationInfo;

public interface AuthenticationInfoService  extends BaseService<AuthenticationInfo> {

}
