package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseEntiyService;
import com.xiaoteng.domin.ResponsePage;
import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.domin.SourceResponse;
import com.xiaoteng.domin.UserResponse;
import com.xiaoteng.entity.User;

import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 *
 */
public interface UserService extends BaseEntiyService<User> {

    @Transactional
    public User save(User request);

    @Transactional
    public void update(User request);

    @Transactional
    public void updateUserDeduction(User request);

    @Transactional
    public void del(String id);

    public SourceResponse get(String id);


    public User findUserByUserName(String username);
    public User getUser(String username);

    public List<User> getUsers(User request);

    public Integer modifyPassword(Long userId, String originalPassword, String newPassword);

    public User auth(String username, String password);
    public User getByMobile(String mobile);
    //认证使用
    public UserResponse getFindByMobile(String mobile);
    List<User> getAll();
    ResponsePage findUserListByproperties(SearchPage searchPage);
    ResponsePage statisticalAnalysisList(SearchPage searchPage);
}
