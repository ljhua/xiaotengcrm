package com.xiaoteng.service;



import java.util.List;

/**
 * Role Service接口
 */
public interface UserRoleService {

    public Integer deleteUserRole(Long userId, Long roleId);

    public Integer changeUserRole(Long userId, List<Long> targetRoles);
    public Integer deleteUserRoleByuserId(Long userId);
}
