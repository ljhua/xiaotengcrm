package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.AuthenticationBill;

public interface AuthenticationBillService extends BaseService<AuthenticationBill> {

    public AuthenticationBill findByUserId(String userId);
}
