package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.BankInfo;

import java.util.List;

public interface BankInfoService extends BaseService<BankInfo> {
    public List<BankInfo> findByUserId(String userId);
}
