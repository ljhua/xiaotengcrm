package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.Resource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Resource Service接口
 */
public interface ResourceService {

    @Transactional
    public Resource save(Resource request);

    @Transactional
    public Resource update(Resource request);

    @Transactional
    public Integer del(Long id);

    public Resource get(Long id);

    public List<Resource> getResources();

    public List<Resource> getResources(Long roleId);

    public List<Resource> getResources(Long parentId, String type);

    public List<Resource> getResourceByUsername(String username, String type);
}
