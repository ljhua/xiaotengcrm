package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.IdCardAuthen;
import org.springframework.web.multipart.MultipartFile;

public interface IdCardAuthenService extends BaseService<IdCardAuthen> {
    public  IdCardAuthen findByUserId(String userid);
 }
