package com.xiaoteng.service;


import com.xiaoteng.core.service.BaseService;
import com.xiaoteng.entity.Contact;

import java.util.List;
import java.util.Map;


public interface ContactService  extends BaseService<Contact> {
    public List<Map<String,Object>> countAllByByUserId(String userId);
    public List<Contact> findByUserId(String userId);
}
