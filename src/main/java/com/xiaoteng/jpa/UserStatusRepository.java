package com.xiaoteng.jpa;


import com.xiaoteng.entity.UserStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface UserStatusRepository extends JpaRepository<UserStatus, String>,JpaSpecificationExecutor<UserStatus> {
    @Query(value = "update user_status set userstatus=?1 where userid=?2 ", nativeQuery = true)
    @Modifying
    public void updateUserStatus(String userstatus,String userid);

    UserStatus findByUserId(String userid);
}
