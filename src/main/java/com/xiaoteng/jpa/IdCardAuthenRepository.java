package com.xiaoteng.jpa;

import com.xiaoteng.entity.IdCardAuthen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface IdCardAuthenRepository extends JpaRepository<IdCardAuthen, String>,JpaSpecificationExecutor<IdCardAuthen> {
    public  IdCardAuthen findByUserId(String userid);
   }
