package com.xiaoteng.jpa;

import com.xiaoteng.entity.LiveBody;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface LiveBodyRepository extends JpaRepository<LiveBody, String>,JpaSpecificationExecutor<LiveBody> {
    public  LiveBody findByUserId(String userid);

}
