package com.xiaoteng.jpa;

import com.xiaoteng.entity.AuthenticationBill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface AuthenticationBillRepository  extends JpaRepository<AuthenticationBill, String>,JpaSpecificationExecutor<AuthenticationBill> {
    @Query(value = "select * from authentication_bill t where t.userid = ?1",nativeQuery = true)
    public AuthenticationBill findByUserId(String userId);
}
