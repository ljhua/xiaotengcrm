package com.xiaoteng.jpa;

import com.xiaoteng.entity.AuthenticationTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface AuthenticationTaskRepository  extends JpaRepository<AuthenticationTask, String>,JpaSpecificationExecutor<AuthenticationTask> {
}
