package com.xiaoteng.jpa;


import com.xiaoteng.entity.BankInfo;
import com.xiaoteng.entity.User;
import com.xiaoteng.entity.UserStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Repository
public interface UserModelBeanRepository extends JpaRepository<User, String>,JpaSpecificationExecutor<User> {
    public List<User> findByMobileAndPassword(String mobile,String password);
    public List<User> findByMobile(String mobile);


    @Query(value = "update sys_user set status=?1 ,mobile=?2,name=?3," +
            "update_user=?4,update_time=?5,is_deduction=?6,deduction_ratio=?7,address=?8,promoters=?9 " +
            " where id=?10 ", nativeQuery = true)
    @Modifying
    public void update(int status,String mobile,String name,String updateUser,
                       Date updateTime,int isDeduction,BigDecimal deductionRatio,String address,String promoters,String id);

    @Query(value = "update sys_user set update_user=?1,update_time=?2,is_deduction=?3,deduction_ratio=?4 " +
            "where id=?5 ", nativeQuery = true)
    @Modifying
     void updateUserDeduction(String updateUser, Date updateTime,int isDeduction,BigDecimal deductionRatio ,String id);

}
