package com.xiaoteng.jpa;

import com.xiaoteng.entity.AuthenticationBill;
import com.xiaoteng.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ContactRepository extends JpaRepository<Contact, String>,JpaSpecificationExecutor<Contact> {

    @Query(value = "select COUNT(t.user_id) count,DATE_FORMAT(t.update_date,'%Y-%m-%d') updatetime from contact t where t.user_id = ?1 GROUP BY t.user_id,updatetime",nativeQuery = true)
    public List<Map<String,Object>> countByUserId(String userId);
    public List<Contact> findByUserId(String userId);
 }
