package com.xiaoteng.jpa;


import com.xiaoteng.entity.Repayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepaymentRepository extends JpaRepository<Repayment, String>,JpaSpecificationExecutor<Repayment> {
   public List<Repayment> findByUserId(String userId);
}
