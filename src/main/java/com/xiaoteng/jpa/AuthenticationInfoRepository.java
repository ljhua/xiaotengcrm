package com.xiaoteng.jpa;

import com.xiaoteng.entity.AuthenticationInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthenticationInfoRepository  extends JpaRepository<AuthenticationInfo, String>,JpaSpecificationExecutor<AuthenticationInfo> {
    public AuthenticationInfo findByUserId(String userId);

}
