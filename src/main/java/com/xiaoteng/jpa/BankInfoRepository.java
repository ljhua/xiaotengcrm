package com.xiaoteng.jpa;

import com.xiaoteng.entity.BankInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BankInfoRepository extends JpaRepository<BankInfo, String>,JpaSpecificationExecutor<BankInfo> {
    public List<BankInfo> findByUserId(String userId);
}
