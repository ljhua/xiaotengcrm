package com.xiaoteng.jpa;

import com.xiaoteng.entity.EmergencyPerson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface EmergencyPersonRepository  extends JpaRepository<EmergencyPerson, String>,JpaSpecificationExecutor<EmergencyPerson> {
    public EmergencyPerson findByUserId(String userId);

}
