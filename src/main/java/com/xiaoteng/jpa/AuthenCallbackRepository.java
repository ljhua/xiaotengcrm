package com.xiaoteng.jpa;

import com.xiaoteng.entity.AuthenCallback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface AuthenCallbackRepository extends JpaRepository<AuthenCallback, String>,JpaSpecificationExecutor<AuthenCallback> {

    @Query(value = "select * from authen_callback t where t.userid = ?1",nativeQuery = true)
    public AuthenCallback findByUserId(String userId);
}
