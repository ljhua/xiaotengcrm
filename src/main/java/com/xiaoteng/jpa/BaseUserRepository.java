package com.xiaoteng.jpa;

import com.xiaoteng.entity.BaseUser;
import com.xiaoteng.entity.UseInfoProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface BaseUserRepository extends JpaRepository<BaseUser, String>,JpaSpecificationExecutor<BaseUser> {

}
