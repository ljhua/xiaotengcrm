package com.xiaoteng.api.authorization.resolver;


import com.xiaoteng.authorization.repository.UserModelRepository;
import com.xiaoteng.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author yummy
 * @create 2018-11-23
 */

@Component
public class UserRepository implements UserModelRepository {

    @Autowired
    private UserService userService;

    @Override
    public Object getCurrentUser(String key) {
        return userService.getFindByMobile(key);
    }
}
