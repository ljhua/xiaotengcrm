package com.xiaoteng.core.model;

public class LogLuncene extends BaseModel {
    private String custTime;
    private String statement;
    private String connection;
    private String afterSql;
    private String beforeSql;

    public String getNow() {
        return now;
    }

    public void setNow(String now) {
        this.now = now;
    }

    private String now;
    public void setId(String id) {
        this.id = id;
    }

    public String getCustTime() {
        return custTime;
    }

    public void setCustTime(String custTime) {
        this.custTime = custTime;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public String getAfterSql() {
        return afterSql;
    }

    public void setAfterSql(String afterSql) {
        this.afterSql = afterSql;
    }

    public String getBeforeSql() {
        return beforeSql;
    }

    public void setBeforeSql(String beforeSql) {
        this.beforeSql = beforeSql;
    }


}
