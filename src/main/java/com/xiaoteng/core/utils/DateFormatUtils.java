package com.xiaoteng.core.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateFormatUtils {

    public static Date getLoanEndDate(int type){//七天以后日期
        Date date=null;
        try {
        SimpleDateFormat dfResule=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat dfCenter=new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_WEEK, type);
        String centerTime=dfCenter.format(c.getTime());

            date=  dfResule.parse(centerTime+" 23:59:59");
        } catch (ParseException e) {
            e.printStackTrace();
        }finally {
            return date;
        }

    }

    //data转换为string
    public static String dateToString(Date date,String format){
        //设置要获取到什么样的时间
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        //获取String类型的时间
      return sdf.format(date);
    }
    //data转换为string
    public static Date StringToDate(String dateString,String format){
        //设置要获取到什么样的时间
        Date date=null;
        try {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        //获取String类型的时间
            date =sdf.parse(dateString);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  date;
    }
}
