package com.xiaoteng.core.utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class AuthUtils {
    //测试账号
    //private static String API_KEY = "2c4b3a21eb164eac9fc3fa9eca4f7b8a";
    //private static String API_SECRET = "e3e22565f93045308c65104aaff93688";
    //正式账号
    private static String API_KEY = "236a18ea963511e8bb00021eab61b074";
    private static String API_SECRET = "b5a18997bbc249e6a944555489d7e14c";

    //1、时间戳

    private static String getTimestamp () {
        return System.currentTimeMillis() + "";
    }

    //2. 获得随机nonce，返回nonce： "pG9GJ4RhThLpn1MJ"
    private static synchronized String getUUID() {
        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        String nonce = str.replace("-", "");
        return nonce;
    }

    //3. 将timestamp、nonce、API_KEY 这三个字符串进行升序排列（依据字符串首位字符的ASCII码)，并join成一个字符串，返回join_str:"1513664992431d73c74a348d24dd9813721a7525cdb7epG9GJ4RhThLpn1MJ"
    private static String genjoinstr(String timestamp, String nonce, String API_KEY) {

        ArrayList<String> beforesort = new ArrayList<String>();
        beforesort.add(API_KEY);
        beforesort.add(timestamp);
        beforesort.add(nonce);

        Collections.sort(beforesort, new SpellComparator());
        StringBuffer aftersort = new StringBuffer();
        for (int i = 0; i < beforesort.size(); i++) {
            aftersort.append(beforesort.get(i));
        }

        String join_str = aftersort.toString();
        return join_str;
    }

    //4. 用API_SECRET对join_str做hamc-sha256签名，且以16进制编码，返回signature:"4dae83383ccf9d382cd14e492668c1202c404f5a0fa477a5281043bd2b42f0a9"
    public static String genEncryptString(String join_str, String API_SECRET) {

        Key sk = new SecretKeySpec(API_SECRET.getBytes(), "HmacSHA256");
        Mac mac = null;
        try {
            mac = Mac.getInstance(sk.getAlgorithm());
            mac.init(sk);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        final byte[] hmac = mac.doFinal(join_str.getBytes());//完成hamc-sha256签名
        StringBuilder sb = new StringBuilder(hmac.length * 2);
        Formatter formatter = new Formatter(sb);
        for (byte b : hmac) {
            formatter.format("%02x", b);
        }
        String signature = sb.toString();//完成16进制编码
        return signature;
    }
    //5. 将上述的值按照 #{k}=#{v} 并以 ',' join在一起，返回签名认证字符串：
//"key=d73c74a348d24dd9813721a7525cdb7e,timestamp=1513664992431,nonce=pG9GJ4RhThLpn1MJ,signature=4dae83383ccf9d382cd14e492668c1202c404f5a0fa477a5281043bd2b42f0a9"

    private static String genauthorization(String timestamp, String nonce) {

        String authorization = "key=" + API_KEY
                + ",timestamp=" + timestamp
                + ",nonce=" + nonce
                + ",signature=" + genEncryptString(genjoinstr( timestamp,  nonce,API_KEY),API_SECRET);
        return authorization;
    }
public static String genauthorizationInfo(){
    return genauthorization(getTimestamp(),getUUID());
}
    static class SpellComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            try {
                // 取得比较对象的汉字编码，并将其转换成字符串
                String s1 = new String(o1.toString().getBytes("GB2312"), "UTF-8");
                String s2 = new String(o2.toString().getBytes("GB2312"), "UTF-8");
                // 运用String类的 compareTo（）方法对两对象进行比较
                return s1.compareTo(s2);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        }

    }
}
