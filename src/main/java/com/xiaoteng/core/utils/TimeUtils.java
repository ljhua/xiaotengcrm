package com.xiaoteng.core.utils;

import java.util.TimeZone;

public class TimeUtils {
    public static  String getCurrentYearMonthDD() {
        TimeZone tz = TimeZone.getTimeZone("Etc/GMT-8");
        TimeZone.setDefault(tz);
        java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyyMMdd");
        java.util.Date date = new java.util.Date(new java.util.Date().getTime());
        String currentDate = format.format(date);
        return currentDate;
    }
}
