package com.xiaoteng.core.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 验证码utils
 */
public class CodeUtils {

    static char[] codeSequence = { /*'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
            'X', 'Y', 'Z',*/ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };


    public static Map<String ,Object> sendMessage(String mobile) throws IOException{
        Map<String ,Object> codeMap=new HashMap<>();
        StringBuffer randomCode = new StringBuffer();
        // 创建一个随机数生成器类
        Random random = new Random();
        // 随机产生codeCount数字的验证码。
        int codeCount = 4;
        for (int i = 0; i < codeCount; i++) {
            // 得到随机产生的验证码数字。
            String code = String.valueOf(codeSequence[random.nextInt(10)]);
        // 将产生的四个随机数组合在一起。
            randomCode.append(code);
        }
        String code=randomCode.toString();
        /*
        测试账号：tz15157063162
        密码：Tz950516
         */
        String response="";//OkHttpUtils.run("http://118.178.138.170/msg/HttpBatchSendSM?account=fw5y54&pswd=0JynB7uG&mobile="+mobile+"&msg=尊敬的用户，您的验证码是："+code+"，有效期10分钟，请勿将验证码告知他人。&needstatus=true");
        codeMap.put("code",code);
       String[] responseStrings=response.split("\n");
        String[] responseStringsSplit=responseStrings[0].split(",");
        codeMap.put("response",responseStringsSplit[1]);

        return codeMap;
    }
}
