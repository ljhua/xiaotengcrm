package com.xiaoteng.core.service;


import com.xiaoteng.core.model.BaseModel;
import com.xiaoteng.domin.SearchPage;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface BaseService <T extends BaseModel>{
    void delete(String id);
    T queryById(String id);
    List<T> queryAll();
    Page queryByproperties(SearchPage searchPage);
    T save(T t);

    default void idEnsure(T a) {
        if (a.getId() == null) {
            a.setId(UUID.randomUUID().toString().replace("-", ""));
        }

    }
    default void createOrUpdateDateEnsure(T a) {
        if (a.getCreateDate() == null) {
            a.setCreateDate(new Date());
        }
        if (a.getUpdateDate() == null) {
            a.setUpdateDate(new Date());
        }
    }

}
