package com.xiaoteng.core.service;


import com.xiaoteng.core.model.BaseEnty;
import com.xiaoteng.core.model.BaseModel;
import com.xiaoteng.domin.SearchPage;
import com.xiaoteng.utils.UUID;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;


public interface BaseEntiyService<T extends BaseEnty>{

    default void createOrUpdateDateEnsure(T a) {
        if (a.getCreateTime() == null) {
            a.setCreateTime(new Date());
        }
        if (a.getUpdateTime() == null) {
            a.setCreateTime(new Date());
        }
    }
    default void idEnsure(T a) {
        if (a.getId() == null) {
            a.setId(UUID.generateShortUuid());
        }

    }
    default void updateDateEnsure(T a) {
        a.setCreateTime(new Date());
    }
}
